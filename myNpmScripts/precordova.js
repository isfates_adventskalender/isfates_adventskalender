const fs = require('fs');
const readline = require('readline');
const SETTINGS = require('../app-settings.json');
const ABSOLUTE_PATH_TO_PROJECT = 'C:\\UwAmp\\www\\isfates_adventskalender';
const apiToUse = SETTINGS['api_to_use'];
const useCordova = SETTINGS['use_cordova'];

const deleteFolderRecursive = function (path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file, index) {
            var curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});


if (apiToUse !== 'server') {
    console.log('"api_to_use" parameter is not set to "server" in your "app-settings.json"\n');
    console.log(`> "api_to_use" : "${apiToUse}"`);
    console.log(`                  ${('^').repeat(apiToUse.length)}\n\n`);

    rl.question(">>> Do you want to continue anyway? Yes/No: ", (answer) => {
        if (answer !== "Yes") {
            process.exit(1);
        } else {
            process.exit(0);
        }

        rl.close();
    });
}

if (!useCordova) {
    console.log('"use_cordova" parameter is not set to true in your "app-settings.json"\n');
    console.log(`> "use_cordova" : "${useCordova}"`);
    console.log(`                   ${('^').repeat(String(useCordova).length)}\n\n`);

    rl.question(">>> Do you want to continue anyway? Yes/No: ", (answer) => {
        if (answer !== "Yes") {
            process.exit(1);
        } else {
            process.exit(0);
        }

        rl.close();
    });
} else {
    deleteFolderRecursive(ABSOLUTE_PATH_TO_PROJECT + '/mobileApp/www');
    process.exit(0);
}


