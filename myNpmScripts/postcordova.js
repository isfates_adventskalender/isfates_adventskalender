const fs = require('fs');
const ABSOLUTE_PATH_TO_PROJECT = 'C:\\UwAmp\\www\\isfates_adventskalender';

const deleteFolderRecursive = function (path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file, index) {
            var curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

function insert(str, index, value) {
    return str.substr(0, index) + value + str.substr(index);
}

const indexHtmlContent = fs.readFileSync(ABSOLUTE_PATH_TO_PROJECT + '/mobileApp/www/index.html').toString();
const indexToInjectScript = indexHtmlContent.indexOf("<script src=");

const indexHtmlWithCordovaScript = insert(indexHtmlContent, indexToInjectScript, '<script src="./cordova.js"></script>')

fs.writeFile(ABSOLUTE_PATH_TO_PROJECT + '/mobileApp/www/index.html', indexHtmlWithCordovaScript, (err) => {
    if (err) throw err;
    console.log('> Cordova js script has been include to index.html !');
});