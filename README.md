# ISFATES_Adventskalender

#### \> See online app in development [here](https://test.johannchopin.fr/advents_kalender)

----
### npm CLI:
* `npm run dev` : build project in `/build/dev` folder and open it on server `http://localhost:1234` (**Warning**: all links are absolute)
* `npm run devLocal` : build project with relative links in `/build/dev` folder
* `npm run prod` : build project with relative links and minify code in `/build/prod` folder
* `npm run cordova` : build project like `prod` in `/mobileApp/www` folder and add `cordova.js` script


----
### This project use:
* Web app bundler :
    * [ParcelJs](https://en.parceljs.org/)

* HTML/CSS :
    * [Bootstrap](https://getbootstrap.com/docs/4.2/getting-started/introduction/) for styles
    * [font-awesome](https://fontawesome.com/icons?d=gallery) for icons
    * [Sass](https://sass-lang.com/documentation/file.SASS_REFERENCE.html) to write css styles more quickly
        
* JS :
    * [Jquery](https://api.jquery.com/) to use Bootstrap
    * [ReactJS](https://reactjs.org/docs/getting-started.html) to easily create an user interface


----
### Installation:

1. Edit `app-settings.json` in `root` directory with yours settings
    ```json
    {
        "name": "ISFATES-adventskalender",
        "version": "<version>",
        "use_cordova": "<boolean>",
        "local_path_to_project": "<your/local/path>",
        "path_to_imgs": "<link/to/imgs>",
        "calendar_period": {
            "from": "2019-12-01",
            "to": "2019-12-24"
        },
        "api_to_use": "<local || server>",
        "api": {
            "local": "<link/to/api>",
            "server": "https://api.isfates-adventskalender.eu/src/api/"
        }
    }
    ```