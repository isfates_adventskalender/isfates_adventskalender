import * as React from 'react';
import * as ReactDOM from 'react-dom';

// IMPORT LIBRARIES ZONE
import './assets/lib/jquery';
import './assets/lib/bootstrap';
import './assets/lib/fontawesome';
// END IMPORT LIBRARIES ZONE

// IMPORT STYLES ZONE
import './assets/styles/style.scss';
// END IMPORT STYLES ZONE

// IMPORT STORAGE HANDLER ZONE
import { LocalStorageHandler } from './assets/tools/storageHandler/storageHandler';
// END IMPORT STORAGE HANDLER ZONE

// IMPORT CORDOVA PLUGINS
import ScreenOrientationPlugin from './assets/cordovaPlugins/screenOrientation';
import InAppBrowserPlugin from './assets/cordovaPlugins/inAppBrowser';
import ConnexionStatusPlugin from './assets/cordovaPlugins/connexionStatus';
// END IMPORT CORDOVA PLUGINS

// IMPORT COMPONENTS ZONE
import Modal from './assets/uiComponents/modal/modal';
import SimpleModal from './assets/uiComponents/simpleModal/simpleModal';
import LoadingAnimation from './assets/uiComponents/loadingAnimation/loadingAnimation';
import Splashscreen from './assets/uiComponents/splashscreen/splashscreen';
import ServerErrorModal from './assets/uiComponents/serverErrorModal/serverErrorModal';
import ConnexionStatus from './assets/uiComponents/connexionStatus/connexionStatus';
// END IMPORT COMPONENTS ZONE

// IMPORT PAGES ZONE
import LoginPage from './pages/loginPage/loginPage';
import MainPage from './pages/mainPage/mainPage';
// END IMPORT PAGES ZONE

// IMPORT IMAGES ZONE   
// @ts-ignore
// END IMPORT IMAGES ZONE

// IMPORT HELPER ZONE
import Helper from './helper';
// END IMPORT HELPER ZONE

// IMPORT INTERFACE ZONE
import {
    ISimpleModalParams,
    TPages,
    TLanguages,
    IModalParams,
    TConnexionStatus
} from './commonInterface';
// END IMPORT INTERFACE ZONE


interface IProps { }

interface IState {
    language: TLanguages,
    pageToShow: TPages,
    modalParams: IModalParams,
    simpleModalParams: ISimpleModalParams,
    showSplashscreenPage: boolean,
    connexionStatus: TConnexionStatus,
}

// TODO : Change setState calling
// use -> this.setState(prevState => ({ test: "test" }))
// instead of -> this.setState({ test: "test" })

class App extends React.Component<IProps, IState> {

    protected modalComponent: JQuery;

    constructor(props: IProps) {
        super(props);
        this.state = {
            language: 'en',
            pageToShow: 'mainPage',
            modalParams: {
                title: '',
                body: '',
                onOk: (): void => { },
            },
            simpleModalParams: {
                type: 'danger',
                message: '',
            },
            showSplashscreenPage: false,
            connexionStatus: 'online',
        };
    }

    componentDidMount(): void {
        this.init();
    }

    protected init() {
        this.initUI();
        this.initLanguage();
        this.initCordovaPlugins();
    }

    protected initUI(): void {
        this.modalComponent = $('#modal');
    }

    protected initLanguage = (): void => {
        const storedLanguage = LocalStorageHandler.getItem('language');

        if (Helper.isSet(storedLanguage)) {
            const language = storedLanguage as TLanguages;

            this.setState((prevState: IState) => ({
                language: language,
            }));
        } else {
            LocalStorageHandler.setItem('language', 'en');
        }
    }

    protected initCordovaPlugins = (): void => {
        new ScreenOrientationPlugin();
        new InAppBrowserPlugin();
        new ConnexionStatusPlugin(() => this.setConnexionStatus("online"), () => this.setConnexionStatus("offline"));
    }

    protected goToPage = (pageName: TPages): void => {
        this.setState({
            pageToShow: pageName,
        })
    };

    protected setConnexionStatus = (status: TConnexionStatus): void => {
        this.setState((prevstate: IState) => ({
            connexionStatus: status,
        }));
    }

    protected showModal = (params: IModalParams): void => {
        this.setState((prevstate: IState) => ({
            modalParams: params,
        }), () => {
            this.modalComponent.modal('show');
        });
    }

    protected showSimpleModal = (params: ISimpleModalParams): void => {
        this.setState({
            simpleModalParams: params,
        }, () => {
            // TODO: Refactoring and move to SimpleModal component
            $('#simpleModal').modal();

            if (!params.noAutoHide) {
                // Hide simple modal after 2.3s
                setTimeout(() => {
                    $('#simpleModal').modal('hide');
                }, 2300);
            }
        });

    };

    protected showSplashscreenPage = (showOrHide: boolean): void => {
        this.setState({
            showSplashscreenPage: showOrHide,
        });
    }

    protected setLanguage = (language: TLanguages): void => {
        this.setState({
            language: language,
        });

        LocalStorageHandler.setItem('language', language);
    };

    protected splashscreenPageRender() {
        return (
            <Splashscreen
                show={this.state.showSplashscreenPage}
            />
        )
    }

    protected loginPageRender() {
        return (
            <LoginPage
                language={this.state.language}
                goToPage={this.goToPage}
                setLanguage={this.setLanguage}
                showSimpleModal={this.showSimpleModal}
            />
        );
    }

    protected mainPageRender() {
        return (
            <MainPage
                language={this.state.language}
                goToPage={this.goToPage}
                showSimpleModal={this.showSimpleModal}
                setLanguage={this.setLanguage}
                showSplashscreenPage={this.showSplashscreenPage}
            />
        );
    }

    protected modalsRender() {
        return (
            <div id="modals">
                <SimpleModal
                    params={this.state.simpleModalParams}
                />

                <Modal
                    params={this.state.modalParams}
                />

                <ServerErrorModal
                    language={this.state.language}
                />
            </div>
        );
    }

    protected connexionStatusRender() {
        return (
            <ConnexionStatus
                language={this.state.language}
                status={this.state.connexionStatus}
            />
        )
    }

    render() {
        return (
            <div>
                {this.splashscreenPageRender()}

                <main className="content-wrapper">
                    {
                        (() => {
                            switch (this.state.pageToShow) {

                                case 'loginPage':
                                    return this.loginPageRender();

                                case 'mainPage':
                                    return this.mainPageRender();

                            }
                        })()
                    }
                </main>

                {this.modalsRender()}
                <LoadingAnimation />
                {this.connexionStatusRender()}
            </div>
        )
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('app') as HTMLElement
);