import { IconProp } from '@fortawesome/fontawesome-svg-core';

export interface IDimensions {
    width: number,
    height: number,
}

export interface IToken {
    token: string,
}


export interface IGetUserData {
    email: string,
    password: string,
}

export interface ICreateUserData {
    email: string,
    name: string,
    first_name: string,
    gender: string,
    password: string,
    language: TLanguages,
}

export interface IForgetPwdData {
    email: string,
}

export interface IModalParams {
    title: string,
    body: string,
    okBtnText?: string,
    onOk: () => void,
}

export interface ISimpleModalParams {
    type: TSimpleModalType,
    message: string,
    noAutoHide?: boolean,
    isHtml?: boolean,
}

export interface ISponsor {
    name: string,
    image: string,
    link: string,
}


// API INTERFACES
export interface ISetLangData {
    lang: TLanguages,
}

export interface IChangePwdData {
    token: string,
    pwd: string,
    newPwd: string,
}

export interface ISendEmailToAdminData extends IToken {
    subject: string,
    msg: string,
}

export interface IGetDayGiftData {
    openedDay: number,
}

export interface IRequestResponse {
    success: boolean,
}

export interface ICreateUserAccountResponse extends IRequestResponse {
    response: {
        hasUserBeenCreated: boolean,
        error: string,
    }
}

export interface IIsUserResponse extends IRequestResponse {
    response: {
        isUser: boolean,
        token?: string,
    }
}

export interface IIsTokenValidResponse extends IRequestResponse {
    response: {
        isTokenValid: boolean,
    }
}

export interface IGetUserDataResponse extends IRequestResponse {
    response: {
        pseudo: string,
        email: string,
        name: string,
        first_name: string,
        daysPattern: number[],
        openedDays: number[],
        lang: TLanguages,
        unlockedDays: number[],
        avatarLink: string,
    }
}

export interface ISetLangResponse extends IRequestResponse {
    response: {
        hasLangBeChanged: boolean, // TODO: Check spelling
    }
}

export interface IChangePwdResponse extends IRequestResponse {
    response: {
        hasPwdBeChanged: boolean, // TODO: Check spelling
    }
}

export interface ISendEmailToAdminResponse extends IRequestResponse {
    response: {
        hasEmailBeSend: boolean, // TODO: Check spelling
    }
}

export interface ISendForgetPwdMailResponse extends IRequestResponse {
    response: {
        hasEmailBeSend: boolean, // TODO: Check spelling
        error_type?: number,
    }
}

export interface IGetDayGiftDataResponse extends IRequestResponse {
    response: {
        id: TGiftsId,
        img: string,
        text: string,
        template: string,
        sponsor: ISponsor,
        was_recovered: boolean,
    }
}
// END API INTERFACES


export interface IUserData {
    name: string,
    first_name: string,
    avatarLink: string,
}



// TYPES ZONE
export type TPages = 'loginPage' | 'mainPage';
export type TPagesInMain = 'calendarPage' | 'contactPage' | 'userProfilPage' | 'userGiftsPage' | 'sponsorsPage'
    | 'settingsPage';
export type TLanguages = 'en' | 'fr' | 'de';
export type TSimpleModalType = 'success' | 'warning' | 'danger';
export type TGiftsId = 'gift_000' | 'gift_wallpaper_000' | 'gift_404';
export type TGiftsTemplate = 'single_image' | 'simple' | 'wallpaper' | 'text';
export type TFontawesomeIconClass = IconProp;
export type TBootstrapPlugin = 'tooltips';
export type TScreenOrientation = 'portrait' | 'landscape';
export type TConnexionStatus = 'online' | 'offline';
export type TGender = 'm' | 'f';

export type TPopups = TPopupsInLoginPage | TPopupsInUserProfilPage;
export type TPopupsInLoginPage = 'forgetPwdPopup';
export type TPopupsInUserProfilPage = 'changePwdPopup';
// END TYPES ZONE