const COMMON_LOCALIZE = {
    de: {
        email_has_been_sent: "Ihre E-Mail wurde erfolgreich gesendet !",
        send: "Senden",
        french: "Französisch",
        german: "Deutsch",
        english: "Englisch",
        settings: "Einstellungen",
        language: "Sprache",
        feature_come_soon: "Feature kommt bald !",
        sponsored_by: "gesponsert von",
    },
    fr: {
        email_has_been_sent: "Votre email a été envoyé avec succès !",
        send: "Envoyer",
        french: "Français",
        german: "Allemand",
        english: "Anglais",
        settings: "Paramètres",
        language: "Langue",
        feature_come_soon: "Feature bientôt disponible !",
        sponsored_by: "sponsorisé par",
    },
    en: {
        email_has_been_sent: "Your email has been successfully sent !",
        send: "Send",
        french: "French",
        german: "German",
        english: "English",
        settings: "Settings",
        language: "Language",
        feature_come_soon: "Feature come soon !",
        sponsored_by: "sponsored by",
    },
};

export default COMMON_LOCALIZE;