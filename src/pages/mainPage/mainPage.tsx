import * as React from 'react';


// IMPORT HELPER ZONE
import Helper from '../../helper';
// END IMPORT HELPER ZONE

// IMPORT APICMDS ZONE
import ApiCmds from './../../api/apiCmds';
// END IMPORT APICMDS ZONE

// IMPORT COOKIEHANDLER ZONE
import { CookieHandler, LocalStorageHandler } from '../../assets/tools/storageHandler/storageHandler';
// END IMPORT COOKIEHANDLER ZONE

// IMPORT APP SETTINGS ZONE
import AppSettings from '../../appSettings';
// END IMPORT APP SETTINGS ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize'
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './mainPage.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import PageBase from '../../assets/uiComponents/base/pageBase';
import Navbar from '../../assets/uiComponents/navbar/navbar';
// END IMPORT COMPONENTS ZONE

// IMPORT PAGES ZONE
import CalendarPage from './pages/calendarPage/calendarPage';
import ContactPage from './pages/contactPage/contactPage';
import UserProfilPage from './pages/userProfilPage/userProfilPage';
import UserGiftsPage from './pages/userGiftsPage/userGiftsPage';
import SponsorsPage from './pages/sponsorsPage/sponsorsPage';
import SettingsPage from './pages/settingsPage/settingsPage';
// END IMPORT PAGES ZONE

// IMPORT INTERFACE ZONE
import {
    ISimpleModalParams,
    TPages,
    TLanguages,
    IGetUserDataResponse,
    TPagesInMain,
    IUserData,
    IGetDayGiftDataResponse
} from "../../commonInterface";
// END IMPORT INTERFACE ZONE

interface IProps {
    goToPage: (pageName: TPages) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    setLanguage: (language: TLanguages) => void,
    showSplashscreenPage: (showOrHide: boolean, ) => void,
    language: TLanguages,
}

interface IState {
    pageToShow: TPagesInMain,
    userToken: string,
    userData: IUserData,
    userEmail: string,
    userPseudo: string,
    userAvatarLink: string,
    userDayPattern: number[],
    userOpenedDays: number[],
    userUnlockedDays: number[],
}


export default class MainPage extends PageBase<IProps, IState> {

    protected pageId = 'mainPage';
    protected userStartSeeSplashscreen: number;
    protected SPLASHSCREEN_LIFE_DURATION: number = 900; // Minimum time the splashscreen should be visible to the user in ms
    protected LOCALSTORAGE_GIFTDATA_PREFIX = 'gift_day_';

    constructor(props: IProps) {
        super(props);

        // TODO: Store all user data in on object in state
        this.state = {
            pageToShow: 'calendarPage',
            userToken: '',
            userEmail: '',
            userPseudo: '',
            userData: {
                name: '',
                first_name: '',
                avatarLink: '',
            },
            userAvatarLink: '',
            userDayPattern: [],
            userOpenedDays: [],
            userUnlockedDays: [],
        };
    }

    componentDidMount() {
        this.init();
    }

    protected init(): void {
        this.showLoadingAnimation();
        this.checkCookie();
    }

    protected goToPageInMain = (pageName: TPagesInMain): void => {
        this.setState({
            pageToShow: pageName,
        })
    };

    protected getToLoginPageOnError(): void {
        this.props.goToPage('loginPage');
        this.hideLoadingAnimation();
    }

    protected checkCookie() {
        const token = CookieHandler.getCookie('token');

        if (!Helper.isStringEmpty(token)) {
            this.getUserData();
        } else {
            this.getToLoginPageOnError();
        }
    }

    protected getUserData = (): void => {
        const token = {
            token: CookieHandler.getCookie('token'),
        };

        ApiCmds.getUserData(token, (result: IGetUserDataResponse) => {
            this.onGetUserDataCallback(result);
        })
    }

    protected showLoadingAnimation = (): void => {
        this.userStartSeeSplashscreen = Helper.getTimestamp();
        this.props.showSplashscreenPage(true);
    }

    protected hideLoadingAnimation = (): void => {
        const remainingVisibilityTime = Helper.getTimestamp() - this.userStartSeeSplashscreen + this.SPLASHSCREEN_LIFE_DURATION;

        setTimeout(() => {
            this.props.showSplashscreenPage(false);
        }, remainingVisibilityTime);
    }

    protected onGetUserDataCallback(result: IGetUserDataResponse): void {
        if (result.success) {
            const response = result.response;

            this.setUserToken();
            this.setUserLang(response.lang);

            this.setState((prevState: IState) => ({
                userPseudo: response.pseudo,
                userEmail: response.email,
                userDayPattern: response.daysPattern,
                userUnlockedDays: response.unlockedDays,
                userOpenedDays: response.openedDays,
                userData: {
                    ...prevState.userData,
                    name: response.name,
                    first_name: response.first_name,
                    avatarLink: response.avatarLink
                }
            }), () => {
                this.storeGiftsDataLocally();
                this.hideLoadingAnimation();
            });
        } else {
            this.getToLoginPageOnError();
        }
    }

    protected storeGiftsDataLocally(): void {
        this.state.userOpenedDays.forEach((dayId: number) => {
            const localGiftData = LocalStorageHandler.getItem('gift_day_' + dayId);

            if (!Helper.isSet(localGiftData)) {
                ApiCmds.getDayGiftData({ openedDay: dayId }, (response: IGetDayGiftDataResponse) => {
                    this.getDayGiftDataCallback(response, dayId);
                });
            }
        });
    }

    protected getDayGiftDataCallback = (response: IGetDayGiftDataResponse, dayId: number): void => {
        if (response.success) {
            this.storeGiftDataToLocalStorage(response.response, dayId);
        }
    }

    protected storeGiftDataToLocalStorage = (giftData: IGetDayGiftDataResponse['response'], dayId: number) => {
        LocalStorageHandler.setItem(this.LOCALSTORAGE_GIFTDATA_PREFIX + dayId, giftData);
    }

    protected setUserToken = (): void => {
        this.setState({
            userToken: CookieHandler.getCookie('token'),
        });
    }

    protected setUserLang = (lang: TLanguages): void => {
        this.props.setLanguage(lang);
    }

    protected logoutUser = (): void => {
        CookieHandler.deleteCookie('token');
        this.props.goToPage('loginPage');
    }

    protected canGiftBeOpen = (): boolean => {
        return (this.state.userUnlockedDays.filter(x => !this.state.userOpenedDays.includes(x))).length !== 0;
    }


    protected navbarRender() {
        return (
            <Navbar
                goToPage={this.goToPageInMain}
                setLanguage={this.props.setLanguage}
                language={this.props.language}
                userData={this.state.userData}
                userName={this.state.userData.name}
                userFirstName={this.state.userData.first_name}
                userAvatarLink={this.state.userAvatarLink}
                logoutUser={this.logoutUser}
            />
        )
    }


    // TODO: Send user token to each pages
    protected calendarPageRender() {
        return <CalendarPage
            language={this.props.language}
            daysPattern={this.state.userDayPattern}
            unlockedDays={this.state.userUnlockedDays}
            openedDays={this.state.userOpenedDays}
            calendarPeriod={AppSettings.settings["calendar_period"]}
        />;
    }

    protected contactPageRender() {
        return (
            <ContactPage
                goToPage={this.goToPageInMain}
                showSimpleModal={this.props.showSimpleModal}
                language={this.props.language}
                userEmail={this.state.userEmail}
                userToken={this.state.userToken}
            />
        )
    }

    protected userProfilPageRender() {
        const userData = {
            pseudo: this.state.userPseudo,
            avatarLink: this.state.userAvatarLink,
        };

        return (
            <UserProfilPage
                goToPage={this.goToPageInMain}
                showSimpleModal={this.props.showSimpleModal}
                language={this.props.language}
                userToken={this.state.userToken}
                userData={this.state.userData}
                logoutUser={this.logoutUser}
            />
        )
    }

    protected userGiftsPageRender() {
        return (
            <UserGiftsPage
                goToPage={this.goToPageInMain}
                showSimpleModal={this.props.showSimpleModal}
                language={this.props.language}
                userToken={this.state.userToken}
                openedDays={this.state.userOpenedDays}
            />
        )
    }

    protected sponsorsPageRender() {
        return (
            <SponsorsPage
                goToPage={this.goToPageInMain}
                showSimpleModal={this.props.showSimpleModal}
                language={this.props.language}
            />
        )
    }

    protected settingsPageRender() {
        return (
            <SettingsPage
                goToPage={this.goToPageInMain}
                showSimpleModal={this.props.showSimpleModal}
                language={this.props.language}
                setLanguage={this.props.setLanguage}
            />
        )
    }


    render() {
        return (
            <div id={this.setPageId()}>
                {this.navbarRender()}

                <div id={this.setIdTo("pages")} className="with-scrollbar">
                    {
                        (() => {
                            switch (this.state.pageToShow) {

                                case 'calendarPage':
                                    return this.calendarPageRender();

                                case 'contactPage':
                                    return this.contactPageRender();

                                case 'userProfilPage':
                                    return this.userProfilPageRender();

                                case 'userGiftsPage':
                                    return this.userGiftsPageRender();

                                case 'sponsorsPage':
                                    return this.sponsorsPageRender();
                                case 'settingsPage':
                                    return this.settingsPageRender();

                            }
                        })()
                    }
                </div>

            </div>
        );
    }
}
