import * as React from 'react';

import { LocalStorageHandler } from '../../../../assets/tools/storageHandler/storageHandler';

// IMPORT HELPER ZONE
import Helper from '../../../../helper';
// END IMPORT HELPER ZONE

// IMPORT APICMDS ZONE
import ApiCmds from '../../../../api/apiCmds';
// END IMPORT APICMDS ZONE

// IMPORT STYLES ZONE
import './userGiftsPage.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import PageBase from '../../../../assets/uiComponents/base/pageBase';
import IconHandler from '~assets/uiComponents/iconHandler/iconHandler';
// END IMPORT COMPONENTS ZONE

// IMPORT COMMON_LOCALIZE ZONE
import LOCALIZE from './localize';
import COMMON_LOCALIZE from '../../../../commonLocalize';
// END IMPORT COMMON_LOCALIZE ZONE

// IMPORT INTERFACE ZONE
import {
    TLanguages,
    ISimpleModalParams,
    TPagesInMain,
    IGetDayGiftDataResponse,
    ISponsor
} from '../../../../commonInterface';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    goToPage: (pageName: TPagesInMain) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    language: TLanguages,
    userToken: string,
    openedDays: number[],
}

interface IState { }

interface IGift {
    id: string,
    imgPath: string,
    text: string,
    sponsor: ISponsor,
    wasRecovered: boolean,
}
// END INTERFACES ZONE


export default class UserGiftsPage extends PageBase<IProps, IState> {

    protected pageId = 'userGiftsPage';

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    protected init(): void {
        this.initUI();
    };

    protected initUI(): void {
        this.initBootstrapPlugins(['tooltips']);
    }
    protected clear(): void { };

    protected getGiftData(dayId: number): IGift {
        const localGiftData = LocalStorageHandler.getItem('gift_day_' + dayId);

        if (Helper.isSet(localGiftData)) {
            return Helper.stringToObject(localGiftData) as IGift;
        }

        return null;
    }

    protected recoveredBadgeRender(): React.ReactNode {
        return (
            <div className="recovered">
                <IconHandler icon="check-circle" />
                <p>{LOCALIZE[this.props.language].recovered}</p>
            </div>
        )
    }

    protected notRecoveredBadgeRender(giftId: string): React.ReactNode {
        return (
            <div
                className="not-recovered clickable"
                data-toggle="tooltip" data-placement="top"
                title={'#' + giftId}
            >
                <IconHandler icon="cart-arrow-down" />
                <p>{LOCALIZE[this.props.language].not_recovered}</p>
            </div>
        )
    }

    protected wasRecoveredRender(wasRecovered: boolean, giftId: string): React.ReactNode {
        return (
            <div>
                {wasRecovered ? this.recoveredBadgeRender() : this.notRecoveredBadgeRender(giftId)}
            </div>
        )
    }

    protected giftListItemRender(i: number, dayId: number): React.ReactNode {
        const localize = LOCALIZE[this.props.language];
        const giftData = this.getGiftData(dayId);

        if (giftData === null) {
            return '';
        }

        const giftText = Helper.stringToObject(giftData.text)[this.props.language];


        return (
            <li key={i}>
                <div className="day">{dayId}</div>
                <div className="sponsor">
                    <p>{giftData.sponsor.name}</p>
                </div>

                {this.wasRecoveredRender(giftData.wasRecovered, giftData.id)}
            </li>
        )
    }

    protected userGiftsListRender(): React.ReactNode {
        return (
            <ul id={this.setIdTo('giftsList')}>
                {Helper.getSortedNumberArray(this.props.openedDays).map((dayId: number, i: number) =>
                    this.giftListItemRender(i, dayId)
                )}
            </ul>
        )
    }


    render() {
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setPageId()} className="page page-transition page-centered">
                <h1>
                    <IconHandler icon="gifts" className="with-pr" />
                    {localize.page_title}
                </h1>
                <hr />

                {this.userGiftsListRender()}
            </div>
        );
    }
}
