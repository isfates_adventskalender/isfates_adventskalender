const LOCALIZE = {
    de: {
        page_title: "Meine Geschenke",
        gift_sponsored_by: "Dieses Geschenk wird gesponsert von ",
        recovered: "bereits erholt",
        not_recovered: "Komm und hol mich ab",
    },
    fr: {
        page_title: "Mes cadeaux",
        gift_sponsored_by: "Ce cadeau est sponsorisé par ",
        recovered: "déjà récupéré",
        not_recovered: "viens me chercher",
    },

    en: {
        page_title: "My gifts",
        gift_sponsored_by: "This gift is sponsored by ",
        recovered: "already recovered",
        not_recovered: "come and get me",
    },
};

export default LOCALIZE;