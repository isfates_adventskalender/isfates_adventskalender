const LOCALIZE = {
    'de': {
        page_title: "Unsere unglaublichen Sponsoren",
    },
    'fr': {
        page_title: "Nos incroyables sponsors",
    },

    'en': {
        page_title: "Our incredible sponsors",
    },
};

export default LOCALIZE;