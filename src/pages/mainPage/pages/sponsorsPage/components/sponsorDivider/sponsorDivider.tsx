import * as React from 'react';


// IMPORT STYLES ZONE
import './sponsorDivider.scss';
// END IMPORT STYLES ZONE

// IMPORT ASSETS ZONE
// END IMPORT ASSETS ZONE


// IMPORT COMPONENTS ZONE 
import ComponentBase from '../../../../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

import IconHandler from '../../../../../../assets/uiComponents/iconHandler/iconHandler';

// INTERFACES ZONE
interface IProps {
    starsNumber: number,
}

interface IState { }

// END INTERFACES ZONE


export default class SponsorDivider extends ComponentBase<IProps, IState> {

    protected componentId = 'sponsorDivider';

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    protected starsRender(): React.ReactNode {
        let stars = [];

        for (let i = 1; i <= this.props.starsNumber; i++) {
            stars.push(<IconHandler key={i} icon="star" />);
        }

        return (
            <div className="sponsor-divider-stars">
                {stars}
            </div>
        )
    }



    render() {

        return (
            <div className="sponsor-divider">
                <div className="sponsor-divider-line"></div>
                {this.starsRender()}
                <div className="sponsor-divider-line"></div>
            </div>
        );
    }
}
