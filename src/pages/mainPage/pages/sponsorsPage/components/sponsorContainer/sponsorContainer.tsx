import * as React from 'react';


// IMPORT STYLES ZONE
import './sponsorContainer.scss';
// END IMPORT STYLES ZONE

// IMPORT ASSETS ZONE
// END IMPORT ASSETS ZONE


// IMPORT COMPONENTS ZONE 
import ComponentBase from '../../../../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

import LinkOpeningHandler from '~assets/tools/linkOpeningHandler';

// INTERFACES ZONE
interface IProps {
    imgSrc: string;
    sponsorName: string;
    link: string;
    className?: string,
}

interface IState { }

// END INTERFACES ZONE


export default class SponsorContainer extends ComponentBase<IProps, IState> {

    protected componentId = 'sponsorContainer';

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    protected getClassName = (): string => {
        const defaultClassAttr = 'sponsor-container clickable clickable-effect';

        if (this.props.className === undefined) {
            return defaultClassAttr;
        }

        return defaultClassAttr + ' ' + this.props.className;
    }



    render(): React.ReactNode {

        return (
            <div
                className={this.getClassName()}
                onClick={() => { LinkOpeningHandler.openLink(this.props.link) }}
            >
                <img src={this.props.imgSrc} />

                <h2>{this.props.sponsorName}</h2>
            </div>
        );
    }
}
