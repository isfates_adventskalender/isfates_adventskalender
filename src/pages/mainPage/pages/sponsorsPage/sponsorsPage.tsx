import * as React from 'react';


// IMPORT APICMDS ZONE
// END IMPORT APICMDS ZONE

// IMPORT STYLES ZONE
import './sponsorsPage.scss';
// END IMPORT STYLES ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import DivaLogo from '../../../../assets/img/sponsors/Diva.png';
// @ts-ignore
import FoderverreinLogo from '../../../../assets/img/sponsors/Foderverrein.png';
// @ts-ignore
import IsfatesLogo from '../../../../assets/img/sponsors/Isfates.png';
// @ts-ignore
import FlyingTigerLogo from '../../../../assets/img/sponsors/FlyingTiger.png';
// @ts-ignore
import LushLogo from '../../../../assets/img/sponsors/Lush.png';
// @ts-ignore
import DouglasLogo from '../../../../assets/img/sponsors/Douglas.png';
// @ts-ignore
import SaarlandThermenLogo from '../../../../assets/img/sponsors/Thermes.png';
// @ts-ignore
import StudiolineLogo from '../../../../assets/img/sponsors/Studioline.png';
// @ts-ignore
import MoulesFripesLogo from '../../../../assets/img/sponsors/MoulesFripes.png';
// @ts-ignore
import ExitAdventuresLogo from '../../../../assets/img/sponsors/ExitAdventures.png';
// @ts-ignore
import TeeStrickerLogo from '../../../../assets/img/sponsors/TeeStricker.png';
// @ts-ignore
import YvesRocherLogo from '../../../../assets/img/sponsors/YvesRocher.png';
// @ts-ignore
import BodyShopLogo from '../../../../assets/img/sponsors/BodyShop.png';
// END IMPORT IMAGE ZONE

// IMPORT COMPONENTS ZONE
import IconHandler from '../../../../assets/uiComponents/iconHandler/iconHandler';
import PageBase from '../../../../assets/uiComponents/base/pageBase';
import SponsorDivider from './components/sponsorDivider/sponsorDivider';
import SponsorContainer from './components/sponsorContainer/sponsorContainer';
// END IMPORT COMPONENTS ZONE

// IMPORT COMMON_LOCALIZE ZONE
import COMMON_LOCALIZE from '../../../../commonLocalize';
// END IMPORT COMMON_LOCALIZE ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize'
// END IMPORT LOCALIZE ZONE

import LinkOpeningHandler from '../../../../assets/tools/linkOpeningHandler';

// IMPORT INTERFACE ZONE
import {
    TLanguages,
    ISimpleModalParams, TPagesInMain
} from '../../../../commonInterface';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    goToPage: (pageName: TPagesInMain) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    language: TLanguages,
}

interface IState { }

// END INTERFACES ZONE


export default class SponsorsPage extends PageBase<IProps, IState> {

    protected pageId = 'sponsorsPage';
    protected linkOpeningHandler: LinkOpeningHandler;

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    protected init(): void { }

    protected clear(): void { }


    render(): React.ReactNode {
        const commonLocalize = COMMON_LOCALIZE[this.props.language];
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setPageId()} className="page page-transition page-centered">
                <h1>
                    <IconHandler icon="handshake" className="with-pr" />
                    {localize.page_title}
                </h1>
                <hr />

                <div className={"sponsors-container"}>
                    <SponsorContainer
                        link="https://www.dfhi-isfates.eu/fr/les-amis-de-lisfates/membres/"
                        sponsorName="Förderverrein"
                        imgSrc={FoderverreinLogo}
                        className="gold"
                    />
                    <SponsorContainer
                        link="https://www.dfhi-isfates.eu/fr/a-propos-de-lisfates/"
                        sponsorName="ISFATES"
                        imgSrc={IsfatesLogo}
                    />
                    <SponsorContainer
                        link={"http://diva.dfhi-isfates.eu/"}
                        sponsorName="DIVA"
                        imgSrc={DivaLogo}
                    />
                    <SponsorContainer
                        link="https://de.flyingtiger.com/"
                        sponsorName="Flying Tiger"
                        imgSrc={FlyingTigerLogo}
                    />
                    <SponsorContainer
                        link="https://de.lush.com/shop-saarbruecken-st-johanner-markt"
                        sponsorName="Lush"
                        imgSrc={LushLogo}
                    />
                    <SponsorContainer
                        link="https://www.saarland-therme.de/fr/tarifs.php"
                        sponsorName="Saarland Thermen"
                        imgSrc={SaarlandThermenLogo}
                    />
                    <SponsorContainer
                        link="https://www.douglas.de/Parf%C3%BCmerie/Saarbr%C3%BCcken/0014/0002/vst.html"
                        sponsorName="Douglas"
                        imgSrc={DouglasLogo}
                    />
                    <SponsorContainer
                        link="https://www.studioline.de/fotostudios/saarbruecken-europa-galerie/"
                        sponsorName="Studioline Photography"
                        imgSrc={StudiolineLogo}
                    />
                    <SponsorContainer
                        link="https://www.instagram.com/moulesfripes/?hl=fr"
                        sponsorName="Moules Fripes"
                        imgSrc={MoulesFripesLogo}
                    />
                    <SponsorContainer
                        link=" https://www.exit-adventures.de/saarbruecken/"
                        sponsorName="Exit Adventures"
                        imgSrc={ExitAdventuresLogo}
                    />
                    <SponsorContainer
                        link="https://www.europagalerie.de/fr/boutiques/shops/tee-stricker/?location=p40-1"
                        sponsorName="Tee Stricker"
                        imgSrc={TeeStrickerLogo}
                    />
                    <SponsorContainer
                        link="https://www.yves-rocher.de/schoenheitsgeschaefte/saarland/saarbruecken/saarbruecken/S-DEYROC279"
                        sponsorName="Yves Rocher"
                        imgSrc={YvesRocherLogo}
                    />
                    <SponsorContainer
                        link="https://www.europagalerie.de/fr/boutiques/shops/the-body-shop/?location=p37-8"
                        sponsorName="The Body Shop"
                        imgSrc={BodyShopLogo}
                    />
                </div>

            </div>
        );
    }
}
