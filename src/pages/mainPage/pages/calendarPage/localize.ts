const LOCALIZE = {
    'de': {
        page_title: "Mein Adventskalender",
        merry_christmas: "Fröhliche Weihnachten 🎄",
        ia_thanks: "Das Adventskalender-Team wünscht Ihnen ein frohes Weihnachtsfest und einen schönen Urlaub. Dieses Projekt hat mehr als <b>600€</b> für verschiedene Vereine gesammelt, dank Ihnen und allen Teilnehmern des Adventskalender. Wir danken Ihnen für Ihre Geste und hoffen, Sie im nächsten Jahr wiederzusehen. 😉 <br/><br/>Dank 🎁",
    },
    'fr': {
        page_title: "Mon calendrier de l'avent",
        merry_christmas: "Joyeux Noël 🎄",
        ia_thanks: "La team Adventskalender vous souhaite un Joyeux Noël ainsi que de passer de bonnes vacances. Ce projet a permis de récolter plus de <b>600€</b> pour différentes associations et cela grâce à toi et à tous les participants de l'Adventskalender. Nous te remercions pour ton geste et espérons te revoir l'année prochaine. 😉 <br/><br/>Merci 🎁",
    },

    'en': {
        page_title: "My Advent calendar",
        merry_christmas: "Merry Christmas 🎄",
        ia_thanks: "The Adventskalender team wishes you a Merry Christmas and a happy holiday. This project has raised more than <b>600€</b> for different associations, thanks to you and all the participants of the Adventskalender. We thank you for your gesture and hope to see you again next year. 😉 <br/><br/>Thanks 🎁",
    },
};

export default LOCALIZE;