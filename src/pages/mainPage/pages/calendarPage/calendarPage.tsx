import * as React from 'react';


// IMPORT HELPER ZONE
import Helper from '../../../../helper';
// END IMPORT HELPER ZONE


// IMPORT STYLES ZONE
import './calendarPage.scss';
// END IMPORT STYLES ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize'
// END IMPORT LOCALIZE ZONE

// IMPORT COMPONENTS ZONE
import PageBase from '../../../../assets/uiComponents/base/pageBase';
import GiftPopup from './components/giftPopup/giftPopup';
import CalendarDay from './components/day/calendarDay';
import StartCountdown from './components/startCountdown/startCountdown';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACES ZONE
import { TLanguages } from '~commonInterface';

interface ICalendarPeriod {
    from: string,
    to: string,
}

interface IProps {
    language: TLanguages,
    daysPattern: number[],
    unlockedDays: number[],
    openedDays: number[],
    calendarPeriod: ICalendarPeriod,
}

interface IState {
    showEvent: boolean,
    showStartCounter: boolean,
    selectedDay: number,
    showGiftPopup: boolean,
    clickedGiftDay: number,
}
// END IMPORT INTERFACES ZONE


export default class CalendarPage extends PageBase<IProps, IState> {

    protected pageId = "calendarPage";

    constructor(props: IProps) {
        super(props);

        this.state = {
            showEvent: false,
            showStartCounter: false,
            selectedDay: null,
            showGiftPopup: false,
            clickedGiftDay: null,
        };
    }

    protected init(): void {
        this.showOrHideStartCountdownComponent();
        this.showOrHideMerryChristmasModal();
    }

    protected clear(): void { };

    hideCalendarDays = (selectedDay: number) => {
        this.setState({ showEvent: true });
        this.setState({ selectedDay: selectedDay });
    };

    showCalendarDays = () => {
        this.setState({ showEvent: false });
        this.setState({ selectedDay: 0 });
    };

    protected isDayLocked = (dayId: number): boolean => {
        return !Helper.isInArray(this.props.unlockedDays, dayId);
    }

    protected isDayOpen = (dayId: number): boolean => {
        return Helper.isInArray(this.props.openedDays, dayId);
    }

    protected showOrHideMerryChristmasModal = (): void => {
        const todayDayNumber = Helper.getDate();

        if (todayDayNumber === 24) {
            setTimeout(() => {
                $('#merryChristmasModal').modal('show')
            }, 2000);
        }
    }

    // Don't need giftDate to hide giftPopup -> reason why it's optionnal
    protected showGiftPopup = (showIt: boolean, giftDate?: number): void => {
        this.setState({
            showGiftPopup: showIt,
        });


        if (!Helper.isUndefined(giftDate)) {
            this.setGiftDay(giftDate);
        }
    }

    protected setGiftDay = (giftDate): void => {
        this.setState({
            clickedGiftDay: giftDate,
        });
    }

    protected setShowStartCountdown = (show: boolean): void => {
        this.setState({
            showStartCounter: show,
        });
    }

    protected showOrHideStartCountdownComponent = (): void => {
        const startTimestamp = Helper.dateToTimestamp(this.props.calendarPeriod.from);
        const now = Helper.getTimestamp();

        if (now < startTimestamp) {
            this.setState((prevState: IState) => ({
                showStartCounter: true,
            }))
        }
    }

    protected calendarDaysRender(daysPattern: number[]) {
        return daysPattern.map(
            (dayIndex) => {
                const dayId = Number(dayIndex);

                return (
                    <CalendarDay
                        key={dayIndex}
                        day={dayId}
                        isLocked={this.isDayLocked(dayId)}
                        isOpen={this.isDayOpen(dayId)}
                        hideCalendarDays={this.hideCalendarDays}
                        showGiftPopup={this.showGiftPopup}
                    />
                )
            }
        );
    }

    protected giftPopupRender() {
        return (
            <GiftPopup
                language={this.props.language}
                showGiftPopup={this.state.showGiftPopup}
                clickedGiftDay={this.state.clickedGiftDay}
                setShowGiftPopup={this.showGiftPopup}
            />
        )
    }

    protected componentsRender = () => {
        return (
            <StartCountdown
                language={this.props.language}
                show={this.state.showStartCounter}
                setShow={this.setShowStartCountdown}
                calendarStartDate={this.props.calendarPeriod.from}
            />
        )
    }

    protected merryChristmasModalRender = (): React.ReactNode => {
        const localize = LOCALIZE[this.props.language];

        return (
            <div className="modal fade" id="merryChristmasModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h3 className="modal-title" id="exampleModalLabel">{localize.merry_christmas}</h3>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p dangerouslySetInnerHTML={{ __html: localize.ia_thanks }}></p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }


    render() {
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setPageId()} className="page page-transition">
                <h1 id={this.setIdTo('title')}>{localize.page_title}</h1>
                <div id={this.setIdTo("calendar")} className="container">
                    {this.calendarDaysRender(this.props.daysPattern)}
                </div>

                {this.giftPopupRender()}
                {this.componentsRender()}
                {this.merryChristmasModalRender()}
            </div>
        );
    }
}
