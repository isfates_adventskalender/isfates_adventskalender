import * as React from 'react';


// IMPORT STYLES ZONE
import './calendarDay.scss';
// END IMPORT STYLES ZONE

// IMPORT ASSETS ZONE
// @ts-ignore
import GiftSvg from './img/gift.svg';
// @ts-ignore
import OpenedGiftSvg from './img/openedGift.svg';
// @ts-ignore
import GiftParticule1 from './img/particule1.svg';
// @ts-ignore
import GiftParticule2 from './img/particule2.svg';
// @ts-ignore
import GiftParticule3 from './img/particule3.svg';
// @ts-ignore
import GiftParticule4 from './img/particule4.svg';
// @ts-ignore
import GiftParticule5 from './img/particule5.svg';
// @ts-ignore
import GiftParticule6 from './img/particule6.svg';
// @ts-ignore
import GiftParticule7 from './img/particule7.svg';
// END IMPORT ASSETS ZONE


// IMPORT COMPONENTS ZONE 
import ComponentBase from '../../../../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE


// IMPORT INTERFACE ZONE
import { } from '../../../../../../commonInterface';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    day: number,
    isLocked: boolean,
    isOpen: boolean,
    hideCalendarDays: (selectedDay: number) => void,
    showGiftPopup: (showIt: boolean, giftDate?: number) => void,
}

interface IState {
    isOpen: boolean,
    withOpeningAnimation: boolean,
    withIsLockedAnimation: boolean,
}

// END INTERFACES ZONE


export default class CalendarDay extends ComponentBase<IProps, IState> {

    protected componentId = 'calendarDay';
    protected GIFT_OPEN_ANIMATION_DURATION = 1000;
    protected GIFT_LOCKED_ANIMATION_DURATION = 500;
    protected POPUP_APPARITION_ANIMATION_DURATION = 1000; // TODO: Set POPUP_APPARITION_ANIMATION_DURATION

    constructor(props: IProps) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            withOpeningAnimation: false,
            withIsLockedAnimation: false,
        };
    }


    protected onDayClick = () => {
        if (!this.props.isLocked) {
            this.setState((prevState: IState) => ({
                withOpeningAnimation: true,
            }), () => this.props.showGiftPopup(true, this.props.day));

            setTimeout(() => {
                this.setState((prevState: IState) => ({
                    withOpeningAnimation: false,
                    isOpen: true,
                }));

            }, this.GIFT_OPEN_ANIMATION_DURATION + this.POPUP_APPARITION_ANIMATION_DURATION);
        } else {
            this.setState((prevState: IState) => ({
                withIsLockedAnimation: true,
            }));

            setTimeout(() => {
                this.setState((prevState: IState) => ({
                    withIsLockedAnimation: false,
                }));

            }, this.GIFT_LOCKED_ANIMATION_DURATION);
        }
    }

    protected giftIconRender = () => {
        if (this.state.isOpen) {
            return <OpenedGiftSvg />
        }

        return <GiftSvg />
    }

    protected giftParticulesRender = () => {
        if (this.props.isLocked || this.state.isOpen) {
            return '';
        }

        return (
            // TODO: Better use a loop
            <div id={this.setIdTo("giftParticules")}>
                <GiftParticule1 />
                <GiftParticule2 />
                <GiftParticule3 />
                <GiftParticule4 />
                <GiftParticule5 />
                <GiftParticule6 />
                <GiftParticule7 />
                <GiftParticule1 />
                <GiftParticule2 />
                <GiftParticule3 />
                <GiftParticule4 />
                <GiftParticule5 />
                <GiftParticule6 />
                <GiftParticule7 />
            </div>
        );
    }

    protected getGiftClass = (): string => {
        return "calender-day clickable day-" + this.props.day + ' ' +
            (this.props.isLocked ? 'locked' : '') + ' ' +
            (this.state.isOpen ? 'open' : '') + ' ' +
            (this.state.withIsLockedAnimation ? 'locked-animation' : '') + ' ' +
            (this.state.withOpeningAnimation ? 'opening-animation' : '');
    }

    render() {

        return (
            <div
                className={this.getGiftClass()}
                onClick={() => { this.onDayClick() }}
            >

                {this.giftIconRender()}
                {this.giftParticulesRender()}
                <h3>{this.props.day}</h3>
            </div>
        );
    }
}
