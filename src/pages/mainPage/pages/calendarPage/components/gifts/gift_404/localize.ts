const LOCALIZE = {
    de: {
        title: "Oaaw, es scheint, dass du heute kein großes Glück hast...",
    },
    fr: {
        title: "Ouups, il semble que vous n'ayez pas eu beaucoup de chance aujourd'hui...",
    },

    en: {
        title: "Ouupsi, it seems you're not very lucky today...",
    },
};

export default LOCALIZE;