import * as React from 'react';


// IMPORT HELPER ZONE
// END IMPORT HELPER ZONE

// IMPORT STYLES ZONE
import './gift_404.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../../../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import OpenedEmptyGiftIcon from '../../../../../../../assets/img/openedEmptyGift.svg';
// END IMPORT IMAGE ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT INTERFACE ZONE
import { TLanguages } from '../../../../../../../commonInterface';
// END IMPORT INTERFACE ZONE

// INTERFACES ZONE
interface IProps {
    language: TLanguages,
}

interface IState { }
// END INTERFACES ZONE


export default class Gift_404 extends ComponentBase<IProps, IState> {

    protected componentId = 'gift_404';

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    componentDidMount(): void {
        this.init();
    }

    protected init = (): void => { }


    render() {
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setComponentId()}>
                <div className="opened-empty-gift">
                    <OpenedEmptyGiftIcon />
                </div>

                <h1>{localize.title}</h1>
                <p></p>
            </div >
        );
    }
}
