import * as React from 'react';


// IMPORT HELPER ZONE
// END IMPORT HELPER ZONE

// IMPORT STYLES ZONE
import './gift_wallpaper.scss';
import { TLanguages } from '../../../../../../../commonInterface';
// END IMPORT STYLES ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize'
// END IMPORT LOCALIZE ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../../../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACE ZONE
// END IMPORT INTERFACE ZONE

// INTERFACES ZONE
interface IProps {
    language: TLanguages,
    text: Object,
    img: Object,
}

interface IState { }
// END INTERFACES ZONE


export default class Gift_wallpaper extends ComponentBase<IProps, IState> {

    protected componentId = 'gift_wallpaper';

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    protected getWallpaperSrc(): string {
        const imgs = this.props.img;

        // TODO: Refactoring
        return imgs[Object.keys(imgs)[0]]['name'];
    }


    render() {
        const localize = LOCALIZE[this.props.language];
        const text = this.props.text[this.props.language];

        return (
            <div id={this.setComponentId()}>
                <h1 className="mb-3">{localize['won_wallpaper_title']}</h1>


                <img src={this.getWallpaperSrc()} id={this.setIdTo('wallpaper')}></img>
            </div >
        );
    }
}
