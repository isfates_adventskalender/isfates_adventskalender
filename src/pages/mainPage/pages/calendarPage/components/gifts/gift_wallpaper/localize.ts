const LOCALIZE = {
    'de': {
        won_wallpaper_title: "Herzlichen Glückwunsch, dass du dieses Hintergrundbild gewonnen hast !",

    },
    'fr': {
        won_wallpaper_title: "Félicitations vous avez gagné ce fond d'écran !",
    },

    'en': {
        won_wallpaper_title: "Congratulations you won this wallpaper !",
    },
};

export default LOCALIZE;