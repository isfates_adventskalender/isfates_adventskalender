const LOCALIZE = {
    'de': {
        day: "Tage",
        hour: "Stunde",
        minute: "Minuten",
        second: "Sekunden",

    },
    'fr': {
        day: "Jours",
        hour: "Heures",
        minute: "Minutes",
        second: "Secondes",
    },

    'en': {
        day: "Days",
        hour: "Hours",
        minute: "Minutes",
        second: "Seconds",
    },
};

export default LOCALIZE;