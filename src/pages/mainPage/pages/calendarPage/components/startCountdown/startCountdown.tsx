import * as React from 'react';

// IMPORT HELPER ZONE
import Helper from '../../../../../../helper';
// END IMPORT HELPER ZONE

// IMPORT STYLES ZONE
import './startCountdown.scss';
// END IMPORT STYLES ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT IMAGE ZONE
// END IMPORT IMAGE ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACES ZONE
import { TLanguages } from '~commonInterface';

interface ICountdown {
    days: number,
    hours: number,
    minutes: number,
    seconds: number,
}

interface IProps {
    language: TLanguages
    show: boolean,
    calendarStartDate: string,
    setShow: (show: boolean) => void,
}

interface IState {
    countdown: ICountdown,
}
// END IMPORT INTERFACES ZONE


export default class StartCountdown extends ComponentBase<IProps, IState> {

    protected componentId = 'startCountdown';
    protected countdown: NodeJS.Timeout;

    constructor(props: IProps) {
        super(props);

        this.state = {
            countdown: {
                days: 0,
                hours: 0,
                minutes: 0,
                seconds: 0,
            },
        };
    }

    protected init(): void {
        this.initCountdown();
    }

    protected clear(): void {
        clearInterval(this.countdown);
    }

    protected initCountdown(): void {
        const initCountdownDate = Helper.dateToTimestamp(this.props.calendarStartDate);

        this.countdown = setInterval(() => {
            // Code example -> https://www.w3schools.com/howto/howto_js_countdown.asp
            const now = Helper.getTimestamp();
            const distance = initCountdownDate - now;

            // If the countdown is finished 
            if (distance < 0) {
                clearInterval(this.countdown);
                this.props.setShow(false);
            }

            const days = Math.floor(distance / (60 * 60 * 24));
            const hours = Math.floor((distance % (60 * 60 * 24)) / (60 * 60));
            const minutes = Math.floor((distance % (60 * 60)) / 60);
            const seconds = Math.floor(distance % 60);

            this.setState((prevState: IState) => ({
                countdown: {
                    days: days,
                    hours: hours,
                    minutes: minutes,
                    seconds: seconds,
                },
            }))
        }, 1000)
    }


    render(): React.ReactNode {
        const localize = LOCALIZE[this.props.language];
        const countdown = this.state.countdown;

        if (!this.props.show) {
            return '';
        }

        return (
            <div id={this.setComponentId()}>
                <div className="countdown">
                    <div className="countdown-elmt">
                        <h2>{countdown.days}</h2>
                        <p>{localize.day}</p>
                    </div>
                    <div className="countdown-elmt">
                        <h2>{countdown.hours}</h2>
                        <p>{localize.hour}</p>
                    </div>
                    <div className="countdown-elmt">
                        <h2>{countdown.minutes}</h2>
                        <p>{localize.minute}</p>
                    </div>
                    <div className="countdown-elmt">
                        <h2>{countdown.seconds}</h2>
                        <p>{localize.second}</p>
                    </div>
                </div>
            </div>
        );
    }
}
