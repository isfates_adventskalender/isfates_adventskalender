import * as React from 'react';


// IMPORT HELPER ZONE
// END IMPORT HELPER ZONE

// IMPORT STYLES ZONE
import './simple.scss';
// END IMPORT STYLES ZONE

// IMPORT LOCALIZE ZONE
// END IMPORT LOCALIZE ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../../../../../../assets/uiComponents/base/componentBase';
import Sponsor from '../components/sponsor/sponsor';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACE ZONE
import { TLanguages, ISponsor } from '../../../../../../../../commonInterface';
// END IMPORT INTERFACE ZONE

// INTERFACES ZONE
interface IProps {
    language: TLanguages,
    text,
    img: string,
    sponsor: ISponsor,
}

interface IState { }
// END INTERFACES ZONE


export default class SimpleTemplate extends ComponentBase<IProps, IState> {

    protected componentId = 'simpleTemplate';

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    protected complementTextRender = (): React.ReactNode => {
        if (this.props.text[this.props.language]['complement'] !== undefined) {
            return <div className="complement" dangerouslySetInnerHTML={{ __html: this.props.text[this.props.language]['complement'] }}></div>
        }

        return '';
    }


    render() {
        const text = this.props.text[this.props.language];

        return (
            <div id={this.setComponentId()}>
                <div dangerouslySetInnerHTML={{ __html: text['title'] }}></div>

                <img src={this.props.img}></img>

                {this.complementTextRender()}

                <Sponsor
                    language={this.props.language}
                    name={this.props.sponsor.name}
                    link={this.props.sponsor.link}
                    imagePath={this.props.sponsor.image}
                />
            </div >
        );
    }
}
