import * as React from 'react';


// IMPORT HELPER ZONE
// END IMPORT HELPER ZONE

// IMPORT STYLES ZONE
import './sponsor.scss';
// END IMPORT STYLES ZONE

// IMPORT LOCALIZE ZONE
import COMMON_LOCALIZE from '../../../../../../../../../commonLocalize';
// END IMPORT LOCALIZE ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../../../../../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACE ZONE
import { TLanguages } from '../../../../../../../../../commonInterface';
// END IMPORT INTERFACE ZONE

// INTERFACES ZONE
interface IProps {
    language: TLanguages,
    name: string,
    link: string,
    imagePath: string,
}

interface IState { }
// END INTERFACES ZONE


export default class Sponsor extends ComponentBase<IProps, IState> {

    protected componentId = 'giftSponsor';

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }


    render(): React.ReactNode {
        const commonLocalize = COMMON_LOCALIZE[this.props.language];

        return (
            <a id={this.setComponentId()} href={this.props.link} target="_blank">
                <p>
                    {commonLocalize.sponsored_by}:
                    <span>{this.props.name}</span>
                </p>
            </a >
        );
    }
}
