import * as React from 'react';
import IconHandler from '../../../../../../assets/uiComponents/iconHandler/iconHandler';
import MarkdownConverterHandler from '../../../../../../assets/tools/markdownConverterHandler';


// IMPORT APP SETTINGS ZONE
import AppSettings from '../../../../../../appSettings';
// END IMPORT APP SETTINGS ZONE

// IMPORT HELPER ZONE
import Helper from '../../../../../../helper';
// END IMPORT HELPER ZONE

// IMPORT STORAGE HANDLER ZONE
import { LocalStorageHandler } from '../../../../../../assets/tools/storageHandler/storageHandler';
// END IMPORT STORAGE HANDLER ZONE

// IMPORT APICMDS ZONE
import ApiCmds from '../../../../../../api/apiCmds';
// END IMPORT APICMDS ZONE


// IMPORT STYLES ZONE
import './giftPopup.scss';
// END IMPORT STYLES ZONE


// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACE ZONE
import {
    IGetDayGiftDataResponse,
    TGiftsId,
    TLanguages,
    TGiftsTemplate,
    ISponsor
} from '~commonInterface';
// END IMPORT INTERFACE ZONE

// IMPORT GIFTS ZONE
import SimpleTemplate from './templates/simple/simple';
import TextTemplate from './templates/text/text';
import WallpaperTemplate from './templates/wallpaper/wallpaper';
import Gift_wallpaper from '../gifts/gift_wallpaper/gift_wallpaper';
import Gift_404 from '../gifts/gift_404/gift_404';
// END IMPORT GIFTS ZONE

// INTERFACES ZONE
interface IProps {
    language: TLanguages,
    showGiftPopup: boolean,
    setShowGiftPopup: (showIt: boolean) => void,
    clickedGiftDay: number,
}

interface IState {
    withDisparitionAnimation: boolean,
    giftToShowId: TGiftsId | null,
    giftToShowTemplate: TGiftsTemplate,
    giftToShowText,
    giftToShowImg,
    giftToShowSponsor: ISponsor,
}

// END INTERFACES ZONE


export default class GiftPopup extends ComponentBase<IProps, IState> {

    protected componentId = 'giftPopup';
    protected PATH_TO_IMGS = AppSettings.settings['path_to_imgs'];
    protected LOCALSTORAGE_GIFTDATA_PREFIX = 'gift_day_';
    protected DISPARITION_ANIMATION_DURATION = 1000;
    protected markdownConverterHandler: MarkdownConverterHandler;

    constructor(props: IProps) {
        super(props);

        this.markdownConverterHandler = new MarkdownConverterHandler();

        this.state = {
            withDisparitionAnimation: false,
            giftToShowId: null,
            giftToShowTemplate: null,
            giftToShowText: null,
            giftToShowImg: null,
            giftToShowSponsor: {
                image: '',
                name: '',
                link: '',
            },
        };
    }

    protected init = (): void => { }

    protected getDayGiftData = (day: number): void => {
        const storedGiftData = LocalStorageHandler.getItem(this.LOCALSTORAGE_GIFTDATA_PREFIX + this.props.clickedGiftDay);

        // Check that there is already data stored for clicked gift
        if (storedGiftData !== null) {
            this.setLocalDayGiftData(storedGiftData)
        } else {
            const dayGiftData = {
                openedDay: day,
            }

            ApiCmds.getDayGiftData(dayGiftData, this.getDayGiftDataCallback);
        }
    }

    protected setLocalDayGiftData = (localDayGiftData: string): void => {
        const extractedStoredGiftData = Helper.stringToObject(localDayGiftData);

        this.setState((prevState: IState) => ({
            giftToShowId: extractedStoredGiftData['id'],
            giftToShowTemplate: extractedStoredGiftData['template'],
            giftToShowText: this.getTextsHasHtml(Helper.stringToObject(extractedStoredGiftData['text'])),
            giftToShowImg: Helper.stringToObject(extractedStoredGiftData['img']),
            giftToShowSponsor: (extractedStoredGiftData['sponsor']) as ISponsor,
        }));
    }

    protected getDayGiftDataCallback = (response: IGetDayGiftDataResponse): void => {
        if (response.success) {
            this.setState((prevState: IState) => ({
                giftToShowId: response.response.id,
                giftToShowTemplate: response.response.template as TGiftsTemplate,
                giftToShowText: this.getTextsHasHtml(Helper.stringToObject(response.response.text)),
                giftToShowImg: Helper.stringToObject(response.response.img),
                giftToShowSponsor: response.response.sponsor,
            }));

            this.storeGiftDataToLocalStorage(response.response);
        }
    }

    protected getTextsHasHtml = (texts) => {
        for (let language in texts) {
            for (let textTag in texts[language]) {
                texts[language][textTag] = this.markdownConverterHandler.toHtml(texts[language][textTag]);
            }
        }

        return texts;
    }

    protected storeGiftDataToLocalStorage = (giftData: IGetDayGiftDataResponse['response']) => {
        LocalStorageHandler.setItem(this.LOCALSTORAGE_GIFTDATA_PREFIX + this.props.clickedGiftDay, giftData);
    }

    protected setAbsolutImgsUrl(dayImgResponse: Object): Object {
        for (const imgId in dayImgResponse) {
            dayImgResponse[imgId]['name'] = this.PATH_TO_IMGS + dayImgResponse[imgId]['name'];
        };

        return dayImgResponse;
    }

    protected giftRender = () => {
        switch (this.state.giftToShowTemplate) {

            case 'simple':
                return (
                    <SimpleTemplate
                        language={this.props.language}
                        text={this.state.giftToShowText}
                        img={this.state.giftToShowImg['gift']}
                        sponsor={this.state.giftToShowSponsor}
                    />
                )

            case 'text':
                return (
                    <TextTemplate
                        language={this.props.language}
                        text={this.state.giftToShowText}
                        sponsor={this.state.giftToShowSponsor}
                    />
                )

            case 'wallpaper':
                return (
                    <WallpaperTemplate
                        language={this.props.language}
                        text={this.state.giftToShowText}
                        img={this.state.giftToShowImg['gift']}
                        sponsor={this.state.giftToShowSponsor}
                    />
                )
        }
    }

    protected goBack = (): void => {
        this.setState((prevstate: IState) => ({
            withDisparitionAnimation: true,
        }))

        setTimeout(() => {
            this.props.setShowGiftPopup(false);

            this.setState((prevstate: IState) => ({
                withDisparitionAnimation: false,
            }))
        }, this.DISPARITION_ANIMATION_DURATION);
    }

    protected getGiftPopupClass = (): string => {
        return 'page with-scrollbar no-p-t ' +
            (this.state.withDisparitionAnimation ? 'diparition-animation' : '');
    }

    // Call Api again for new clicked day
    componentDidUpdate(prevProps: IProps) {
        if (prevProps.clickedGiftDay !== this.props.clickedGiftDay) {
            this.getDayGiftData(this.props.clickedGiftDay);
        }
    }

    render() {
        if (!this.props.showGiftPopup) {
            return '';
        }

        return (
            <div id={this.setComponentId()} className={this.getGiftPopupClass()}>
                <IconHandler icon="arrow-left" className="go-back-icon clickable" onClick={() => this.goBack()} />

                <div id={this.setIdTo("content")}>
                    {this.giftRender()}
                </div>
            </div >
        );
    }
}
