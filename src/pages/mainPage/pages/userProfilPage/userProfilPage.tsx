import * as React from 'react';
import IconHandler from '../../../../assets/uiComponents/iconHandler/iconHandler';
import { LocalStorageHandler } from '../../../../assets/tools/storageHandler/storageHandler';

// IMPORT STYLES ZONE
import './userProfilPage.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONEù
import PageBase from '../../../../assets/uiComponents/base/pageBase';
import Popup from '../../../../assets/uiComponents/base/popup/popup';
import ChangePwdPopup from './components/popups/changePwdPopup/changePwdPopup';
// END IMPORT COMPONENTS ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
import COMMON_LOCALIZE from '../../../../commonLocalize';
// END IMPORT LOCALIZE ZONE

// IMPORT INTERFACE ZONE
import {
    TLanguages,
    ISimpleModalParams,
    TPagesInMain,
    IUserData,
} from '../../../../commonInterface';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    goToPage: (pageName: TPagesInMain) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    logoutUser: () => void,
    language: TLanguages,
    userToken: string,
    userData: IUserData,
}

interface IState {
    isChangePwdPopupOpen: boolean,
}
// END INTERFACES ZONE


export default class UserProfilPage extends PageBase<IProps, IState> {

    protected pageId = 'userProfilPage';

    constructor(props: IProps) {
        super(props);

        this.state = {
            isChangePwdPopupOpen: false,
        };
    }

    protected init(): void {
        this.initBootstrapPlugins(['tooltips']);
    };

    protected clear(): void { };

    protected deleteUserAccount = (): void => {
        // TODO: Do stuff to delete user account
    }

    protected showUserToken(): void {
        const localize = LOCALIZE[this.props.language];

        this.props.showSimpleModal({
            type: 'success',
            message: localize.my_token + ': ' + this.props.userToken,
            noAutoHide: true,
        })
    }

    protected clearLocalData(): void {
        const localize = LOCALIZE[this.props.language];

        for (let key in localStorage) {
            if (key.startsWith("IA_")) {
                LocalStorageHandler.deleteItem(key, false);
            }
        }

        this.props.showSimpleModal({
            type: 'success',
            message: localize.data_cleared,
        })
    }

    protected popupsRender(): React.ReactNode {
        const localize = LOCALIZE[this.props.language];

        return (
            <div className="popups-ctn">
                <Popup
                    id="changePwdPopup"
                    title={localize.changePwdPopupTitle}
                    isOpen={this.state.isChangePwdPopupOpen}
                    onPopupClose={() => {
                        this.setState((prevState: IState) => ({ isChangePwdPopupOpen: false }));
                    }}
                >
                    <ChangePwdPopup
                        language={this.props.language}
                        showSimpleModal={this.props.showSimpleModal}
                        logoutUser={this.props.logoutUser}
                    />
                </Popup>
            </div>
        )
    }


    render() {
        const localize = LOCALIZE[this.props.language];
        const userData = this.props.userData;
        const commonLocalize = COMMON_LOCALIZE[this.props.language];

        return (
            <div id={this.setPageId()} className="page page-transition">

                <div id={this.setIdTo("avatar")}>
                    <img src={userData.avatarLink} />
                </div>

                <h1>{userData.first_name + " " + userData.name}</h1>

                <div id={this.setIdTo("advancedSection")} className="card section">
                    <div className="card-body">
                        <h5 className="card-title">{localize.advanced}</h5>
                        <p className="card-text">{localize.advanced_complement}</p>
                    </div>
                    <a
                        href={"#" + this.setIdTo("advancedSectionCollapse")}
                        className="btn btn-danger"
                        data-toggle="collapse"
                        aria-controls={this.setIdTo("advancedSectionCollapse")}
                    >
                        <IconHandler icon="chevron-down" />
                    </a>
                </div>

                <div className="collapse" id={this.setIdTo("advancedSectionCollapse")}>
                    <div>
                        <button type="button" className="btn btn-secondary" onClick={() => this.clearLocalData()}>
                            <IconHandler icon="toilet" className="with-pr" />
                            {localize.clear_local_data}
                        </button>
                        <button type="button" className="btn btn-warning" onClick={() => this.showUserToken()}>
                            <IconHandler icon="tag" className="with-pr" />
                            {localize.see_token}
                        </button>

                        <button type="button" className="btn btn-primary" onClick={() => this.setState((prevState: IState) => ({ isChangePwdPopupOpen: true }))}>
                            <IconHandler icon="key" className="with-pr" />
                            {localize.change_pwd}
                        </button>

                        <button
                            type="button"
                            className="btn btn-danger"
                            onClick={() => this.deleteUserAccount()}
                            data-toggle="tooltip"
                            data-placement="right"
                            title={commonLocalize.feature_come_soon}
                        >
                            <IconHandler icon="trash" className="with-pr" />
                            {localize.delete_account}
                        </button>
                    </div>
                </div>

                {this.popupsRender()}
            </div>
        );
    }
}
