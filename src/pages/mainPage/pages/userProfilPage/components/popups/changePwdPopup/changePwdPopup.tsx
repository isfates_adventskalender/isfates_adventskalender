import * as React from 'react';
import IconHandler from '../../../../../../../assets/uiComponents/iconHandler/iconHandler';


// IMPORT HELPER ZONE
import Helper from '../../../../../../../helper';
// END IMPORT HELPER ZONE

// IMPORT APICMDS ZONE
import ApiCmds from './../../../../../../../api/apiCmds';
// END IMPORT APICMDS ZONE

// IMPORT COOKIEHANDLER ZONE
import { CookieHandler } from '../../../../../../../assets/tools/storageHandler/storageHandler';
// END IMPORT COOKIEHANDLER ZONE

// IMPORT LOCALIZE ZONE
import COMMON_LOCALIZE from '../../../../../../../commonLocalize';
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './changePwdPopup.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACE ZONE
import {
    ISimpleModalParams,
    TLanguages,
    IChangePwdData,
    IChangePwdResponse
} from "../../../../../../../commonInterface";
// END IMPORT INTERFACE ZONE

interface IProps {
    showSimpleModal: (params: ISimpleModalParams) => void,
    language: TLanguages,
    logoutUser: () => void,
}

interface IState {
    textInCurrentPwdInput: string,
    textInNewPwdInput: string,
    textInConfirmNewPwdInput: string,
    translator: any;
    areGivenPasswordsSimilar: boolean,
}


export default class ChangePwdPopup extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);

        this.state = {
            textInCurrentPwdInput: '',
            textInNewPwdInput: '',
            textInConfirmNewPwdInput: '',
            translator: LOCALIZE[this.props.language],
            areGivenPasswordsSimilar: false,
        };
    }

    protected onCurrentPwdInputChange = (event): void => {
        this.setState({
            textInCurrentPwdInput: event.target.value,
        });
    }

    protected onNewPwdInputChange = (event): void => {
        this.setState({
            textInNewPwdInput: event.target.value,
        }, () => {
            this.updatePasswordInputsStatus();
        });
    }

    protected onConfirmNewPwdInputChange = (event): void => {
        this.setState({
            textInConfirmNewPwdInput: event.target.value,
        }, () => {
            this.updatePasswordInputsStatus();
        });
    }

    protected areGivenPasswordsNotSimilar(): boolean {
        const textInConfirmPasswordInput = this.state.textInConfirmNewPwdInput;

        if (!Helper.isStringEmpty(textInConfirmPasswordInput) && textInConfirmPasswordInput !== this.state.textInNewPwdInput) {
            return true;
        }

        return false;
    }

    protected updatePasswordInputsStatus = () => {
        this.setState({
            areGivenPasswordsSimilar: this.areGivenPasswordsNotSimilar(),
        })
    }

    protected onKeyPress = (event): void => {
        if (event.key === 'Enter') {
            this.onSubmitBtnClick();
        }
    }

    protected areInputsValid = (): boolean => {
        if (this.state.textInNewPwdInput !== this.state.textInConfirmNewPwdInput) {
            this.props.showSimpleModal({
                type: 'danger',
                message: LOCALIZE[this.props.language]['passwordsNotSame'],
            })

            return false;
        }

        if (this.state.textInCurrentPwdInput === this.state.textInNewPwdInput) {
            this.props.showSimpleModal({
                type: 'danger',
                message: LOCALIZE[this.props.language]['samePwdHasOldOneError'],
            })

            return false;
        }

        if (Helper.isStringEmpty(this.state.textInNewPwdInput) || Helper.isStringEmpty(this.state.textInConfirmNewPwdInput)) {
            this.props.showSimpleModal({
                type: 'danger',
                message: "Please enter a valid password !",
            })

            return false;
        }

        return true;
    }

    protected getUserData = (): IChangePwdData => {
        return {
            token: CookieHandler.getCookie('token'),
            pwd: Helper.hashStringMd5(this.state.textInCurrentPwdInput),
            newPwd: Helper.hashStringMd5(this.state.textInNewPwdInput),
        }
    }

    protected onSubmitBtnClick() {
        if (this.areInputsValid()) {
            const userData = this.getUserData();

            ApiCmds.changePwd(userData, this.onChangePwdCallback);
        }
    }

    protected onChangePwdCallback = (result: IChangePwdResponse): void => {
        if (result.success) {
            if (result.response.hasPwdBeChanged) {
                this.props.showSimpleModal({
                    type: 'success',
                    message: LOCALIZE[this.props.language]['passwordHasBeenChanged'],
                })

                this.props.logoutUser();
            } else {
                this.props.showSimpleModal({
                    type: 'danger',
                    message: LOCALIZE[this.props.language]['passwordHasNotBeenChanged'],
                })
            }
        }
    }


    public render(): React.ReactNode {
        const localize = LOCALIZE[this.props.language];
        const commonLocalize = COMMON_LOCALIZE[this.props.language];

        return (
            <form className="card card-body">
                <h2>{localize.current_password}</h2>
                <input
                    type="password"
                    autoComplete="current-password"
                    className="form-control"
                    value={this.state.textInCurrentPwdInput}
                    onChange={this.onCurrentPwdInputChange}
                    onKeyPress={this.onKeyPress}
                />

                <h2>{localize.new_password}</h2>
                <input
                    type="password"
                    autoComplete="new-password"
                    className="form-control"
                    value={this.state.textInNewPwdInput}
                    onChange={this.onNewPwdInputChange}
                    onKeyPress={this.onKeyPress}
                />

                <h2>{localize.new_password}</h2>
                <input
                    type="password"
                    autoComplete="new-password"
                    className={"form-control " + (this.state.areGivenPasswordsSimilar ? "invalid-input" : "")}
                    value={this.state.textInConfirmNewPwdInput}
                    onChange={this.onConfirmNewPwdInputChange}
                    onKeyPress={this.onKeyPress}
                />

                <button type="button" className="btn btn-gold" onClick={() => this.onSubmitBtnClick()}>
                    <IconHandler icon="key" className="with-pr" />
                    {commonLocalize['send']}
                </button>
            </form>
        );
    }
}
