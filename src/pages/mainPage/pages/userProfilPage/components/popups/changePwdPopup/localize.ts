const LOCALIZE = {
    de: {
        popupTitle: "Passwort ändern",
        current_password: "Aktuelles Passwort",
        new_password: "Neues Passwort",
        passwordsNotSame: "Neue Passwörter sind nicht gleich !",
        samePwdHasOldOneError: "Das neue Passwort darf nicht mit dem alten übereinstimmen !",
        passwordHasBeenChanged: "Passwort wurde erfolgreich geändert !",
        passwordHasNotBeenChanged: "Das Passwort wurde nicht geändert!",
    },
    fr: {
        popupTitle: "Modifier le mot de passe",
        current_password: "Mot de passe actuel",
        new_password: "Nouveau mot de passe",
        passwordsNotSame: "Les nouveaux mots de passe ne sont pas les mêmes !",
        samePwdHasOldOneError: "Le nouveau mot de passe ne doit pas être le même que l\'ancien !",
        passwordHasBeenChanged: "Le mot de passe a été modifié avec succès !\n Veuillez vous reconnecter à votre compte !",
        passwordHasNotBeenChanged: "Le mot de passe n\'a pas pût être changé !",
    },

    en: {
        popupTitle: "Change the password",
        current_password: "Current password",
        new_password: "New password",
        passwordsNotSame: "Passwords are not the same !",
        samePwdHasOldOneError: "The new password must not be the same as the old one !",
        passwordHasBeenChanged: "Password has been changed successfully !",
        passwordHasNotBeenChanged: "Password has not been changed !",
    },
};

export default LOCALIZE;