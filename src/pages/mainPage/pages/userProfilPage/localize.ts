const LOCALIZE = {
    de: {
        changePwdPopupTitle: "Passwort ändern",
        advanced: "Fortgeschritten",
        advanced_complement: "Passwort ändern, Konto löschen, ...",
        see_token: "Mein Token ansehen",
        change_pwd: "Passwort ändern",
        delete_account: "Konto löschen",
        my_token: "Mein Token",
        clear_local_data: "Lokale Daten löschen",
        data_cleared: "Lokale Daten wurden gelöscht",
    },
    fr: {
        changePwdPopupTitle: "Modifier le mot de passe",
        advanced: "Avancé",
        advanced_complement: "Modifier le mot de passe, Supprimer le compte, ...",
        see_token: "Voir mon token",
        change_pwd: "Modifier le mot de passe",
        delete_account: "Supprimer le compte",
        my_token: "Mon token",
        clear_local_data: "Effacer les données locales",
        data_cleared: "Les données locales ont été supprimées",
    },
    en: {
        changePwdPopupTitle: "Change the password",
        advanced: "Advanced",
        advanced_complement: "Change Password, Delete account, ...",
        see_token: "See my token",
        change_pwd: "Change password",
        delete_account: "Delete account",
        my_token: "My token",
        clear_local_data: "Delete local data",
        data_cleared: "Local data have been deleted",
    },
};

export default LOCALIZE;