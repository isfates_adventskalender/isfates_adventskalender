import * as React from 'react';
import IconHandler from '../../../../assets/uiComponents/iconHandler/iconHandler';

// IMPORT APICMDS ZONE
import ApiCmds from './../../../../api/apiCmds';
// END IMPORT APICMDS ZONE

// IMPORT LOCALIZE ZONE
import COMMON_LOCALIZE from '../../../../commonLocalize';
import LOCALIZE from './localize'
// END IMPORT LOCALIZE ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import FrFlagSvg from '../../../../assets/img/fr.svg';
// @ts-ignore
import DeFlagSvg from '../../../../assets/img/de.svg';
// @ts-ignore
import UsFlagSvg from '../../../../assets/img/us.svg';
// END IMPORT IMAGE ZONE

// IMPORT STYLES ZONE
import './settingsPage.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import PageBase from '../../../../assets/uiComponents/base/pageBase';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACE ZONE
import {
    TLanguages,
    ISimpleModalParams,
    TPagesInMain,
    ISetLangResponse
} from '../../../../commonInterface';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    goToPage: (pageName: TPagesInMain) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    language: TLanguages,
    setLanguage: (language: TLanguages) => void,
}

interface IState {
}

// END INTERFACES ZONE


export default class SettingsPage extends PageBase<IProps, IState> {

    protected pageId = 'settingsPage';

    constructor(props: IProps) {
        super(props);

        this.state = {
        };
    }

    protected init(): void { };
    protected clear(): void { };

    protected setLanguage = (language: TLanguages): void => {
        this.props.setLanguage(language);

        const normalizeLangRequest = {
            lang: language,
        }

        ApiCmds.setLang(normalizeLangRequest,
            (result: ISetLangResponse) => {
                this.onSetLangCallback(result);
            });

    }

    protected onSetLangCallback(result: ISetLangResponse): void { }

    protected getUserLangFlag = (): React.ReactNode => {
        switch (this.props.language) {
            case 'fr':
                return <FrFlagSvg />

            case 'de':
                return <DeFlagSvg />

            case 'en':
                return <UsFlagSvg />
        }
    }

    protected flagsDropdownContentRender = (): React.ReactNode => {
        const selectedLang = this.props.language;
        const commonLocalize = COMMON_LOCALIZE[selectedLang];

        // TODO: Refactoring flagsDropdownContentRender method
        return (
            <div className="dropdown-menu" aria-labelledby={this.setIdTo("langFlagDropdown")}>
                {selectedLang === 'fr' ? '' :
                    <a className="dropdown-item" onClick={() => { this.setLanguage('fr') }}>
                        <FrFlagSvg />
                        {commonLocalize.french}
                    </a>
                }
                {selectedLang === 'de' ? '' :
                    <a className="dropdown-item" onClick={() => { this.setLanguage('de') }}>
                        <DeFlagSvg />
                        {commonLocalize.german}
                    </a>
                }
                {selectedLang === 'en' ? '' :
                    <a className="dropdown-item" onClick={() => { this.setLanguage('en') }}>
                        <UsFlagSvg />
                        {commonLocalize.english}
                    </a>
                }
            </div>
        )
    }


    render() {
        const commonLocalize = COMMON_LOCALIZE[this.props.language];
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setPageId()} className="page page-transition page-centered">
                <h1>
                    <IconHandler icon="cog" className="with-pr" />
                    {commonLocalize.settings}
                </h1>
                <hr />

                <div id={this.setIdTo("settingsCtn")}>
                    <div id={this.setIdTo("language")} className="card section">
                        <div className="card-body">
                            <h2 className="card-title">{commonLocalize.language}</h2>
                        </div>

                        <div className="dropdown clickable">
                            <a
                                id={this.setIdTo("langFlagDropdown")}
                                className="nav-link dropdown-toggle"
                                role="button"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                            >
                                {this.getUserLangFlag()}
                            </a>

                            {this.flagsDropdownContentRender()}

                        </div>
                    </div>

                    <div id={this.setIdTo("user")} className="card section">
                        <div className="card-body">
                            <h2 className="card-title">{localize.user}</h2>
                        </div>

                        <div className="dropdown clickable">
                            <button type="button" className="btn btn-gold" onClick={() => this.props.goToPage('userProfilPage')}>
                                <IconHandler icon="cog" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
