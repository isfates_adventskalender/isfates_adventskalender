const LOCALIZE = {
    de: {
        user: "Benutzer",
    },
    fr: {
        user: "Utilisateur",
    },
    en: {
        user: "User",
    },
};

export default LOCALIZE;