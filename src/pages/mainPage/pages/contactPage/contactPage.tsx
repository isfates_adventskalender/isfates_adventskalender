import * as React from 'react';
import IconHandler from '../../../../assets/uiComponents/iconHandler/iconHandler';

// IMPORT APICMDS ZONE
import ApiCmds from './../../../../api/apiCmds';
// END IMPORT APICMDS ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize'
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './contactPage.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import PageBase from '../../../../assets/uiComponents/base/pageBase';
// END IMPORT COMPONENTS ZONE

// IMPORT COMMON_LOCALIZE ZONE
import COMMON_LOCALIZE from '../../../../commonLocalize';
// END IMPORT COMMON_LOCALIZE ZONE

// IMPORT INTERFACE ZONE
import {
    ISendEmailToAdminData,
    ISendEmailToAdminResponse,
    TLanguages,
    ISimpleModalParams, TPagesInMain
} from '../../../../commonInterface';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    goToPage: (pageName: TPagesInMain) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    language: TLanguages,
    userEmail: string,
    userToken: string,
}

interface IState {
    textInSubjectInput: string,
    textInMsgInput: string,
}

// END INTERFACES ZONE


export default class ContactPage extends PageBase<IProps, IState> {

    protected pageId = 'contactPage';

    constructor(props: IProps) {
        super(props);

        this.state = {
            textInSubjectInput: '',
            textInMsgInput: '',
        };
    }

    protected init(): void { };
    protected clear(): void { };

    protected maxCharactersInMsg = 1024;
    protected maxCharactersInSubject = 30;

    protected onSubjectInputChange = (event): void => {
        this.setState({
            textInSubjectInput: event.target.value,
        });
    }

    protected onMsgInputChange = (event): void => {
        const msg = event.target.value;

        if (msg.length <= this.maxCharactersInMsg) {
            this.setState({
                textInMsgInput: msg,
            });
        }
    }

    protected onKeyPress = (event): void => {
        if (event.key === 'Enter') {
            this.onSubmitBtnClick();
        }
    }

    protected areInputsValid = (): boolean => {
        const textInMsgInputLength = this.state.textInMsgInput.length;
        const textInSubjectInputLength = this.state.textInSubjectInput.length;

        if (textInMsgInputLength > this.maxCharactersInMsg || textInMsgInputLength === 0) {
            return false;
        }

        if (textInSubjectInputLength > this.maxCharactersInSubject || textInSubjectInputLength === 0) {
            return false;
        }

        return true;
    }

    protected getSendEmailToAdminData = (): ISendEmailToAdminData => {
        return {
            token: this.props.userToken,
            subject: this.state.textInSubjectInput,
            msg: this.state.textInMsgInput,
        }
    }

    protected onSubmitBtnClick = (): void => {
        if (this.areInputsValid()) {
            ApiCmds.sendEmailToAdmin(this.getSendEmailToAdminData(), this.onSendEmailToAdminCallback)
        } else {
            this.props.showSimpleModal({
                type: 'danger',
                message: 'Please enter a valid subject and message !',
            })
        }
    }

    protected onSendEmailToAdminCallback = (result: ISendEmailToAdminResponse): void => {
        if (result.response.hasEmailBeSend) {
            this.props.showSimpleModal({
                type: 'success',
                'message': COMMON_LOCALIZE[this.props.language]['email_has_been_sent']
            })

            this.resetInputs();
        } else {
            this.props.showSimpleModal({
                type: 'danger',
                'message': 'Please wait a little bit before sending a new message !', // TODO: Handle error with different way maybe error number
            })
        }
    }

    protected resetInputs = (): void => {
        this.setState({
            textInSubjectInput: '',
            textInMsgInput: '',
        });
    };



    render() {
        const commonLocalize = COMMON_LOCALIZE[this.props.language];
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setPageId()} className="page page-transition page-centered">
                <h1>
                    <IconHandler icon="envelope" className="with-pr" />
                    {localize.page_title}
                </h1>
                <hr />

                <div className="card card-body">
                    <input
                        type="text"
                        className="form-control"
                        value={this.state.textInSubjectInput}
                        onChange={this.onSubjectInputChange}
                        placeholder={localize.subject}
                    />

                    <textarea
                        className="form-control"
                        value={this.state.textInMsgInput}
                        onChange={this.onMsgInputChange}
                        placeholder={localize.message}
                        rows={10}
                    ></textarea>
                    <p id={this.setIdTo("charactersCounter")}>{this.maxCharactersInMsg - this.state.textInMsgInput.length}</p>

                    <button type="button" className="btn btn-gold" onClick={() => this.onSubmitBtnClick()}>
                        <IconHandler icon="paper-plane" className="with-pr" />
                        {commonLocalize['send']}
                    </button>
                </div>
            </div>
        );
    }
}
