const LOCALIZE = {
    de: {
        page_title: "Kontakt",
        subject: "Betreff...",
        message: "Nachricht...",

    },
    fr: {
        page_title: "Contact",
        subject: "Sujet...",
        message: "Message...",
    },
    en: {
        page_title: "Contact",
        subject: "Subject...",
        message: "Message...",
    },
};

export default LOCALIZE;