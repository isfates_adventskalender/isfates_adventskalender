import * as React from 'react';


// IMPORT HELPER ZONE
import Helper from '../../helper';
// END IMPORT HELPER ZONE

// IMPORT STORAGE HANDLER ZONE
import { LocalStorageHandler } from '../../assets/tools/storageHandler/storageHandler';
// END IMPORT STORAGE HANDLER ZONE

// IMPORT STYLES ZONE
import './loginPage.scss';
// END IMPORT STYLES ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT API ZONE
// END IMPORT API ZONE

// IMPORT COMPONENTS ZONE
import PageBase from '../../assets/uiComponents/base/pageBase';
import Popup from '~assets/uiComponents/base/popup/popup';
import LoginForm from './components/loginForm/loginForm';
import SignInForm from './components/signInForm/signInForm';
import ForgetPwdPopup from './components/popups/forgetPwdPopup/forgetPwdPopup';
// END IMPORT COMPONENTS ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import AppIcon from '../../assets/img/icon.svg';
// @ts-ignore
import FrFlagSvg from '../../assets/img/fr.svg';
// @ts-ignore
import DeFlagSvg from '../../assets/img/de.svg';
// @ts-ignore
import UsFlagSvg from '../../assets/img/us.svg';
// END IMPORT IMAGE ZONE

// IMPORT INTERFACE ZONE
import {
    ISimpleModalParams,
    TPages,
    TLanguages
} from '../../commonInterface';
// END IMPORT INTERFACE ZONE

interface IProps {
    language: TLanguages,
    goToPage: (pageName: TPages) => void,
    setLanguage: (language: TLanguages) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
}

interface IState {
    formToShow: string,
    translator: any;
    isForgetPwdPopupOpen: boolean,
}

export default class LoginPage extends PageBase<IProps, IState> {

    protected pageId = 'loginPage';

    constructor(props: IProps) {
        super(props);

        this.state = {
            formToShow: 'logIn',
            translator: LOCALIZE,
            isForgetPwdPopupOpen: false,
        };
    }

    protected init(): void { }
    protected clear(): void { }

    protected goToForm(formToGo: string) {
        this.setState({
            formToShow: formToGo,
        }, () => { this.onGoToFormCallback(formToGo) });
    }

    protected hasUserReadDoc = (): boolean => {
        return Helper.isSet(LocalStorageHandler.getItem('hasUserReadFirstStepDoc'));
    }

    protected onGoToFormCallback = (form: string): void => {
        if (form === 'signIn') {
            if (!this.hasUserReadDoc()) {
                this.scrollToFirstStepDoc();
            }
        }
    }

    protected scrollToFirstStepDoc = (): void => {
        const firstStepDoc = $(this.getId('appPresentation'));

        $('#' + this.getPageId()).animate({
            scrollTop: firstStepDoc.offset().top - 25
        }, 1000);

        LocalStorageHandler.setItem('hasUserReadFirstStepDoc', true);
    }

    protected getUserLangFlag = (): React.ReactNode => {
        switch (this.props.language) {
            case 'fr':
                return <FrFlagSvg />

            case 'de':
                return <DeFlagSvg />

            case 'en':
                return <UsFlagSvg />
        }
    }

    protected languageSelectionRender = (): React.ReactNode => {
        const selectedLang = this.props.language;
        return (
            <div id={this.setIdTo("languageSelection")} className="dropdown clickable">
                <button className="btn btn-gold dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {this.getUserLangFlag()}
                </button>

                <div className="dropdown-menu" aria-labelledby={this.setIdTo("langFlagDropdown")}>
                    {selectedLang === 'fr' ? '' :
                        <a className="dropdown-item" onClick={() => { this.props.setLanguage('fr') }}>
                            <FrFlagSvg />
                        </a>
                    }
                    {selectedLang === 'de' ? '' :
                        <a className="dropdown-item" onClick={() => { this.props.setLanguage('de') }}>
                            <DeFlagSvg />
                        </a>
                    }
                    {selectedLang === 'en' ? '' :
                        <a className="dropdown-item" onClick={() => { this.props.setLanguage('en') }}>
                            <UsFlagSvg />
                        </a>
                    }
                </div>
            </div>
        )
    }

    protected formToShowRender() {
        if (this.state.formToShow === 'signIn') {
            return <SignInForm
                language={this.props.language}
                scrollToFirstStepDoc={this.scrollToFirstStepDoc}
                translator={this.state.translator[this.props.language]}
                showSimpleModal={this.props.showSimpleModal}
            />;
        }

        return <LoginForm
            translator={this.state.translator[this.props.language]}
            goToPage={this.props.goToPage}
            showSimpleModal={this.props.showSimpleModal}
            openForgetPwdPopup={() => this.setState((prevState: IState) => ({ isForgetPwdPopupOpen: true }))}
        />;
    }

    protected popupsRender(): React.ReactNode {
        const localize = LOCALIZE[this.props.language];

        return (
            <div className="popups-ctn">
                <Popup
                    id="forgetPwdPopup"
                    title={localize.forget_pwd_popup_title}
                    isOpen={this.state.isForgetPwdPopupOpen}
                    onPopupClose={() => this.setState((prevState: IState) => ({ isForgetPwdPopupOpen: false }))}
                >
                    <ForgetPwdPopup
                        goToPage={() => { }}
                        language={this.props.language}
                        showSimpleModal={this.props.showSimpleModal}
                    />
                </Popup>
            </div>
        )
    }


    render() {
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setPageId()} className="page page-centered page-transition with-scrollbar">

                <div id={this.setIdTo("iconCtn")}>
                    <AppIcon />
                </div>

                <div id={this.setIdTo('formCtn')}>
                    <div id={this.setIdTo('form')}>
                        <div id={this.setIdTo("buttonCtn")}>
                            <button
                                type="button"
                                onClick={() => this.goToForm('logIn')}
                                className={`btn btn-lg ${this.state.formToShow === 'logIn' ? 'btn-gold' : ''}`}>
                                {this.state.translator[this.props.language].login}
                            </button>
                            <button
                                type="button"
                                onClick={() => this.goToForm('signIn')}
                                className={`btn btn-lg ${this.state.formToShow === 'signIn' ? 'btn-gold' : ''}`}>{this.state.translator[this.props.language].signIn}
                            </button>
                        </div>

                        {this.formToShowRender()}
                    </div>
                </div>

                <div id={this.setIdTo("appPresentation")}>
                    <h2>{localize.app_presentation.title}</h2>

                    <div className="accordion">
                        <div className="card">
                            <div className="card-header clickable" data-toggle="collapse" data-target="#paymentCollapse" aria-expanded="true" aria-controls="paymentCollapse">
                                <h4 className="mb-0">
                                    <b>#1</b> {localize.app_presentation.payment.title}
                                </h4>
                            </div>

                            <div id="paymentCollapse" className="collapse show">
                                <div className="card-body">
                                    <p dangerouslySetInnerHTML={{ __html: localize.app_presentation.payment.explanation }}></p>

                                    <ul>
                                        <li dangerouslySetInnerHTML={{ __html: localize.app_presentation.payment.first + '<p class="warning">' + localize.app_presentation.payment.first_warning + '<p>' }}></li>
                                        <li dangerouslySetInnerHTML={{ __html: localize.app_presentation.payment.second }}></li>
                                    </ul>

                                    <p>{localize.app_presentation.payment.explanation_end}</p>
                                </div>
                            </div>
                        </div>

                        <div className="card">
                            <div className="card-header clickable" data-toggle="collapse" data-target="#inscriptionCollapse" aria-expanded="true" aria-controls="inscriptionCollapse">
                                <h4 className="mb-0">
                                    <b>#2</b> {localize.app_presentation.inscription.title}
                                </h4>
                            </div>

                            <div id="inscriptionCollapse" className="collapse show">
                                <div className="card-body">
                                    <p dangerouslySetInnerHTML={{ __html: localize.app_presentation.inscription.explanation }}></p>
                                </div>
                            </div>
                        </div>

                        <div className="card">
                            <div className="card-header clickable" data-toggle="collapse" data-target="#confirmationCollapse" aria-expanded="true" aria-controls="confirmationCollapse">
                                <h4 className="mb-0">
                                    <b>#3</b> {localize.app_presentation.confirmation.title}
                                </h4>
                            </div>

                            <div id="confirmationCollapse" className="collapse show">
                                <div className="card-body">
                                    <p dangerouslySetInnerHTML={{ __html: localize.app_presentation.confirmation.explanation }}></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h2>{localize.app_presentation.enjoy}</h2>

                </div>

                {this.popupsRender()}
                {this.languageSelectionRender()}
            </div>
        );
    }
}
