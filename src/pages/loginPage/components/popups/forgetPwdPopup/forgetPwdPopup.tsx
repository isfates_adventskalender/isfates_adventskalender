import * as React from 'react';
import IconHandler from '../../../../../assets/uiComponents/iconHandler/iconHandler';


// IMPORT HELPER ZONE
import Helper from '../../../../../helper';
// END IMPORT HELPER ZONE

// IMPORT APICMDS ZONE
import ApiCmds from '../../../../../api/apiCmds';
// END IMPORT APICMDS ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './forgetPwdPopup.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACE ZONE
import {
    ISimpleModalParams,
    TPages,
    TLanguages,
    IForgetPwdData,
    ISendForgetPwdMailResponse,
} from "../../../../../commonInterface";
// END IMPORT INTERFACE ZONE

interface IProps {
    goToPage: (pageName: TPages) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    language: TLanguages,
}

interface IState {
    textInEmailInput: string,
    translator: any;
}


export default class ForgetPwdPopup extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);

        this.state = {
            textInEmailInput: '',
            translator: LOCALIZE[this.props.language],

        };
    }

    protected onEmailInputChange = (event): void => {
        this.setState({
            textInEmailInput: event.target.value,
        });
    }

    protected onKeyPress = (event): void => {
        if (event.key === 'Enter') {
            this.onSubmitBtnClick();
        }
    }

    protected areInputsValid = (): boolean => {
        if (!Helper.checkEmail(this.state.textInEmailInput)) {
            this.props.showSimpleModal({
                type: 'danger',
                message: LOCALIZE[this.props.language]['enter_valid_email'],
            });

            return false;
        }

        return true;
    }

    protected getForgetPwdData(): IForgetPwdData {
        return {
            email: this.state.textInEmailInput
        }
    }

    protected onSubmitBtnClick() {
        if (this.areInputsValid()) {
            ApiCmds.sendForgetPwdMail(this.getForgetPwdData(),
                (result: ISendForgetPwdMailResponse) => {
                    this.onSubmitBtnClickCallback(result);
                }
            );
        }
    }

    protected onSubmitBtnClickCallback = (result: ISendForgetPwdMailResponse): void => {
        const localize = LOCALIZE[this.props.language];

        if (result.success) {
            if (result.response.hasEmailBeSend) {
                // TODO: Clear and close popup

                this.props.showSimpleModal({
                    type: 'success',
                    message: localize.forget_pwd_mail_send,
                });
            } else {
                if (result.response.error_type === 1) {
                    this.props.showSimpleModal({
                        type: 'danger',
                        message: localize.invalid_email,
                        noAutoHide: true,
                    });
                }
                if (result.response.error_type === 2) {
                    this.props.showSimpleModal({
                        type: 'danger',
                        message: localize.email_allready_send,
                        noAutoHide: true,
                    });
                }
            }

        }
    }


    render(): React.ReactNode {
        const localize = LOCALIZE[this.props.language];

        return (
            <div className="card card-body">
                <h4 className="card-title">{localize.title}</h4>
                <p className="card-text" dangerouslySetInnerHTML={{ __html: localize.complements }}></p>

                <input
                    type="text"
                    className="form-control"
                    value={this.state.textInEmailInput}
                    onChange={this.onEmailInputChange}
                    onKeyPress={this.onKeyPress}
                    placeholder={localize['inputEmail']}
                />

                <button type="button" className="btn btn-gold" onClick={() => this.onSubmitBtnClick()}>
                    <IconHandler icon="paper-plane" className="with-pr" />
                    {localize['submit']}
                </button>
            </div>
        );
    }
}
