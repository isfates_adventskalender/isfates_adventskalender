const LOCALIZE = {
    de: {
        title: "Geben Sie Ihre E-Mail-Adresse ein und überprüfen Sie Ihr Konto. Sobald Sie die E-Mail erhalten haben, klicken Sie bitte auf den Link, mit dem Sie ein neues Passwort neu vergeben können.",
        complements: "> Sie müssen sich dann mit diesem neuen Passwort in Ihr Konto einloggen. Bitte ändern Sie dieses temporäre Passwort im Abschnitt <i>Einstellungen</i>.",
        inputEmail: "Deine E-Mail *",
        enter_valid_email: "Bitte geben Sie eine gültige E-Mail-Adresse ein!",
        submit: "Einreichen",
        forget_pwd_mail_send: "Die E-Mail zur Passwortwiederherstellung wurde Ihnen zugesandt !",
        invalid_email: "Dieser E-Mail ist kein Konto zugeordnet",
        email_allready_send: "Eine E-Mail zum Zurücksetzen des Passworts wurde Ihnen bereits zugesandt. Bitte überprüfen Sie Ihr Postfach !",
    },
    fr: {
        title: "Entrez votre email et vérifiez votre compte. Une fois le mail reçu, veuillez cliquer sur le lien qui vous permettra de vous réattribuer un nouveau mot de passe.",
        complements: "> Vous devrez ensuite vous connecter à votre compte avec ce nouveau mot de passe. Veuillez modifier ce mot de passe temporaire dans la section <i>Paramètres</i>.",
        inputEmail: "Votre Email *",
        enter_valid_email: "Veuillez entrer un email valide !",
        submit: "Soumettre",
        forget_pwd_mail_send: "L'email de récupération de mot de passe vous a été envoyé !",
        invalid_email: "Aucun compte n'est associé à cet email",
        email_allready_send: "Un courriel de réinitialisation de mot de passe vous a déjà été envoyé. Veuillez vérifier votre boîte mail !",
    },
    en: {
        title: "Enter your email and check your account. Once you have received the email, please click on the link that will allow you to re-assign a new password.",
        complements: "> You will then need to log into your account with this new password. Please change this temporary password in the <i>Settings</i> section.",
        inputEmail: "Your Email *",
        enter_valid_email: "Please enter a valid email address !",
        submit: "Submit",
        forget_pwd_mail_send: "The password recovery email has been sent to you !",
        invalid_email: "No account is associated with this email",
        email_allready_send: "A password reset email has already been sent to you. Please check your mailbox !",
    },
};

export default LOCALIZE;