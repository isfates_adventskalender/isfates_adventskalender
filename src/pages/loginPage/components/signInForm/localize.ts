const LOCALIZE = {
    de: {
        name: "Dein Name *",
        first_name: "Dein Vorname *",
        serverErrors: {
            accountExist: "Das Konto ist bereits vorhanden",
            notBeneficiary: "Bitte tragen Sie zuerst dazu bei, ein Konto zu erstellen. Lesen Sie die folgende Dokumentation, um mehr zu erfahren ",
            accountInCreation: "Dieses Konto wird bereits angelegt."
        },
        enter_valid_name: "Bitte geben Sie einen gültigen Vor- und Nachnamen ein !",
    },
    fr: {
        name: "Votre nom *",
        first_name: "Votre prénom *",
        serverErrors: {
            accountExist: "Le compte existe déjà",
            notBeneficiary: "Veuillez d'abord contribuer pour créer un compte. Lisez la documentation ci-dessous pour en savoir plus ",
            accountInCreation: "Ce compte est déjà en cours de création"
        },
        enter_valid_name: "Veuillez entrer un prénom et un nom valides !",
    },

    en: {
        name: "Your name *",
        first_name: "Your first name *",
        serverErrors: {
            accountExist: "The account already exists",
            notBeneficiary: "Please contribute first to create an account. Read the documentation below to learn more ",
            accountInCreation: "This account is already being created"
        },
        enter_valid_name: "Please enter a valid first and last name !",
    },
};

export default LOCALIZE;