import * as React from 'react';
import IconHandler from '../../../../assets/uiComponents/iconHandler/iconHandler';


// IMPORT APICMDS ZONE
import ApiCmds from '../../../../api/apiCmds';
// END IMPORT APICMDS ZONE

// INIT HELPER METHODS ZONE
import Helper from '../../../../helper';
// END INIT HELPER METHODS ZONE

// IMPORT STYLES ZONE
import './signInForm.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import ManAvatarIcon from '../../../../assets/img/avatar_man.png';
// @ts-ignore
import WomenAvatarIcon from '../../../../assets/img/avatar_women.png';
// END IMPORT IMAGE ZONE

// IMPORT INTERFACE ZONE
import {
    ICreateUserData,
    ISimpleModalParams,
    ICreateUserAccountResponse,
    TGender,
    TLanguages
} from '../../../../commonInterface';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// IMPORT INTERFACE ZONE

// INTERFACE ZONE
interface IProps {
    language: TLanguages,
    showSimpleModal: (params: ISimpleModalParams) => void,
    scrollToFirstStepDoc: () => void,
    translator: any,
}

interface IState {
    textInEmailInput: string,
    textInUserNameInput: string,
    textInUserFirstNameInput: string,
    selectedGender: TGender,
    textInPasswordInput: string,
    textInConfirmPasswordInput: string,
    areGivenPasswordsSimilar: boolean,
}

// END INTERFACE ZONE


export default class SignInForm extends ComponentBase<IProps, IState> {

    protected componentId = 'signInForm';

    constructor(props: IProps) {
        super(props);

        this.state = {
            textInEmailInput: '',
            textInUserNameInput: '',
            selectedGender: 'f',
            textInUserFirstNameInput: '',
            textInPasswordInput: '',
            textInConfirmPasswordInput: '',
            areGivenPasswordsSimilar: false,
        };
    }

    protected onEmailInputChange = (event) => {
        this.setState({
            textInEmailInput: event.target.value,
        });
    };

    protected onPasswordInputChange = (event) => {
        this.setState({
            textInPasswordInput: event.target.value,
        }, () => {
            this.updatePasswordInputsStatus();
        });
    };

    protected onUserNameInputChange = (event) => {
        this.setState({
            textInUserNameInput: event.target.value,
        });
    };

    protected onUserFirstNameInputChange = (event) => {
        this.setState({
            textInUserFirstNameInput: event.target.value,
        });
    };


    protected onConfirmPasswordInputChange = (event) => {
        this.setState({
            textInConfirmPasswordInput: event.target.value,
        }, () => {
            this.updatePasswordInputsStatus();
        });
    };

    protected areGivenPasswordsNotSimilar(): boolean {
        const textInConfirmPasswordInput = this.state.textInConfirmPasswordInput;

        if (!Helper.isStringEmpty(textInConfirmPasswordInput) && textInConfirmPasswordInput !== this.state.textInPasswordInput) {
            return true;
        }

        return false;
    }

    protected updatePasswordInputsStatus = () => {
        this.setState({
            areGivenPasswordsSimilar: this.areGivenPasswordsNotSimilar(),
        })
    }

    protected onSignInBtnClick = () => {
        if (this.areInputsValid()) {
            const userData = this.getUserData();
            ApiCmds.createUserAccount(userData, (result) => {
                this.onSignInBtnClickCallback(result)
            });
        }
    };

    protected getUserData(): ICreateUserData {
        return {
            email: this.state.textInEmailInput,
            name: this.state.textInUserNameInput,
            first_name: this.state.textInUserFirstNameInput,
            gender: this.state.selectedGender,
            password: Helper.hashStringMd5(this.state.textInPasswordInput),
            language: this.props.language,
        }
    }

    protected onSignInBtnClickCallback(result: ICreateUserAccountResponse) {
        if (result.success) {
            if (result.response.hasUserBeenCreated) {
                this.props.showSimpleModal({
                    type: 'success',
                    message: this.props.translator.checkout_email,
                })
            } else {
                const serverError = result.response.error;
                const errorsMsg = LOCALIZE[this.props.language].serverErrors

                let msgToShow = errorsMsg.notBeneficiary;

                if (serverError === "notBeneficiary") {
                    this.props.scrollToFirstStepDoc();
                }

                if (serverError === "accountInCreation") {
                    msgToShow = errorsMsg.accountInCreation;
                }

                if (serverError === "accountExist") {
                    msgToShow = errorsMsg.accountExist;
                }

                this.props.showSimpleModal({
                    type: 'danger',
                    message: msgToShow,
                    noAutoHide: true,
                })
            }
        }
    }

    protected onKeyPress = (event): void => {
        if (event.key === 'Enter') {
            this.onSignInBtnClick();
        }
    };

    protected selectedGender = (gender: TGender): void => {
        this.setState((prevState: IState) => ({
            selectedGender: gender,
        }));
    }

    protected areInputsValid(): boolean {
        const localize = LOCALIZE[this.props.language];

        if (!Helper.checkEmail(this.state.textInEmailInput)) {
            this.props.showSimpleModal({
                type: 'danger',
                message: this.props.translator.enter_valid_email,
            })

            return false;
        }

        if (Helper.isStringEmpty(this.state.textInUserNameInput)
            || Helper.isStringEmpty(this.state.textInUserFirstNameInput)) {
            this.props.showSimpleModal({
                type: 'danger',
                message: localize.enter_valid_name,
            })

            return false;
        }

        if (this.state.textInPasswordInput !== this.state.textInConfirmPasswordInput) {
            this.props.showSimpleModal({
                type: 'danger',
                message: this.props.translator.passwords_not_same,
            })

            return false;
        }

        return true;
    }

    protected genderInputRender = (): React.ReactNode => {
        return (
            <div id={this.setIdTo("genderInputCtn")} className="mb-3">
                <div
                    id={this.setIdTo("genderInput")}
                    className="clickable"
                >
                    <img
                        src={WomenAvatarIcon}
                        onClick={() => { this.selectedGender('f') }}
                        className={this.state.selectedGender === 'f' ? "gender-avatar selected" : "gender-avatar"}
                    />
                    <img
                        src={ManAvatarIcon}
                        onClick={() => { this.selectedGender('m') }}
                        className={this.state.selectedGender === 'm' ? "gender-avatar selected" : "gender-avatar"}
                    />
                </div>
            </div>
        )
    }


    render() {
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setComponentId()} className="login-container page-transition">
                <div className="login-form-1">
                    <h3>{this.props.translator.signIn} :</h3>
                    <form>
                        {this.genderInputRender()}

                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <IconHandler icon="user" />
                                </span>
                            </div>
                            <input
                                type="text"
                                className="form-control"
                                placeholder={localize.name}
                                value={this.state.textInUserNameInput}
                                onChange={this.onUserNameInputChange}
                                onKeyPress={this.onKeyPress}
                            />
                        </div>

                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <IconHandler icon="user" />
                                </span>
                            </div>
                            <input
                                type="text"
                                className="form-control"
                                placeholder={localize.first_name}
                                value={this.state.textInUserFirstNameInput}
                                onChange={this.onUserFirstNameInputChange}
                                onKeyPress={this.onKeyPress}
                            />
                        </div>

                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <IconHandler icon="envelope" />
                                </span>
                            </div>
                            <input
                                type="email"
                                autoComplete="username"
                                className="form-control"
                                placeholder={this.props.translator.inputEmail}
                                value={this.state.textInEmailInput}
                                onChange={this.onEmailInputChange}
                                onKeyPress={this.onKeyPress}
                            />
                        </div>

                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <IconHandler icon="lock" />
                                </span>
                            </div>
                            <input
                                type="password"
                                autoComplete="new-password"
                                className="form-control"
                                placeholder={this.props.translator.inputPwd}
                                value={this.state.textInPasswordInput}
                                onChange={this.onPasswordInputChange}
                                onKeyPress={this.onKeyPress}
                            />
                        </div>

                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <IconHandler icon="lock" />
                                </span>
                            </div>
                            <input
                                type="password"
                                autoComplete="new-password"
                                className={"form-control " + (this.state.areGivenPasswordsSimilar ? "invalid-input" : "")}
                                placeholder={this.props.translator.inputPwd}
                                value={this.state.textInConfirmPasswordInput}
                                onChange={this.onConfirmPasswordInputChange}
                                onKeyPress={this.onKeyPress}
                            />
                        </div>

                        <div className="form-group flex align-center">

                            <button
                                onClick={this.onSignInBtnClick}
                                type="button" className="btn btn-gold">
                                {this.props.translator.submit}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
