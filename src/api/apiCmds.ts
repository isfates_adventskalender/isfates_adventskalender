// IMPORT API ZONE
import _api from './api'
// END IMPORT API ZONE

// IMPORT COOKIEHANDLER ZONE
import { CookieHandler } from '../assets/tools/storageHandler/storageHandler';
// END IMPORT COOKIEHANDLER ZONE

// IMPORT INTERFACE ZONE
import {
    ICreateUserData,
    ICreateUserAccountResponse,
    IIsUserResponse,
    IToken,
    IIsTokenValidResponse,
    IForgetPwdData,
    IGetUserDataResponse,
    ISetLangData,
    TLanguages,
    ISetLangResponse,
    IChangePwdResponse,
    IChangePwdData,
    ISendEmailToAdminData,
    ISendEmailToAdminResponse,
    ISendForgetPwdMailResponse,
    IGetDayGiftData,
    IGetDayGiftDataResponse,
    IGetUserData
} from '../commonInterface';
// END IMPORT INTERFACE ZONE


export default class ApiCmds {

    protected static Api = new _api();

    public static createUserAccount(data: ICreateUserData, callbackFunction: (response: ICreateUserAccountResponse) => void) {
        this.Api.postRequest('createUserAwaitingConfirmation', data, callbackFunction);
    }

    public static isUser(data: IGetUserData, callbackFunction: (response: IIsUserResponse) => void) {
        this.Api.postRequest('isUser', data, callbackFunction);
    }

    public static isTokenValid(data: IToken, callbackFunction: (response: IIsTokenValidResponse) => void) {
        this.Api.postRequest('isTokenValid', data, callbackFunction);
    }

    public static sendForgetPwdMail(data: IForgetPwdData, callbackFunction: (response: ISendForgetPwdMailResponse) => void) {
        this.Api.postRequest('sendForgetPwdMail', data, callbackFunction);
    }

    public static getUserData(data: IToken, callbackFunction: (response: IGetUserDataResponse) => void) {
        this.Api.postRequest('getUserData', data, callbackFunction);
    }

    public static setLang(data: ISetLangData, callbackFunction: (response: ISetLangResponse) => void) {
        data['token'] = CookieHandler.getCookie("token");

        this.Api.postRequest('setLang', data, callbackFunction);
    }

    public static changePwd(data: IChangePwdData, callbackFunction: (response: IChangePwdResponse) => void) {
        this.Api.postRequest('changePwd', data, callbackFunction);
    }

    public static sendEmailToAdmin(data: ISendEmailToAdminData, callbackFunction: (response: ISendEmailToAdminResponse) => void) {
        this.Api.postRequest('sendEmailToAdmin', data, callbackFunction);
    }

    public static getDayGiftData(data: IGetDayGiftData, callbackFunction: (response: IGetDayGiftDataResponse) => void) {
        data['token'] = CookieHandler.getCookie("token");

        this.Api.postRequest('getDayGiftData', data, callbackFunction, false);
    }

}
