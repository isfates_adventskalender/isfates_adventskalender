import * as Crypto from 'crypto';

export default class Helper {
    public static deleteElementFromArray(array, elementIndex: number) {
        const arrayCopy = array.slice();
        arrayCopy.splice(elementIndex, 1);

        return arrayCopy;
    }

    public static getArrayWithUpdatedElement(array, elementIndex: number, updatedElement) {
        const arrayCopy = array.slice();
        arrayCopy[elementIndex] = updatedElement;

        return arrayCopy;
    }

    public static secondToMilliSeconds(second: number) {
        return second * 1000;
    }

    public static minToMilliSeconds(min: number) {
        return min * this.secondToMilliSeconds(60);
    }

    public static hourToMilliSeconds(hour: number) {
        return hour * this.minToMilliSeconds(60);
    }

    public static milliSecondsToSeconds(ms: number) {
        return ms / 1000;
    }

    public static timestampToValidFullDate(timestamp: number) { // valid full-date take the format YYYY-MM-DD
        const date = new Date(timestamp);
        const day = date.getUTCDate();
        const month = date.getMonth() + 1; // januar is month 0
        const year = date.getFullYear();

        return year + '-' + this.normalizeMonthOrDayString(month) + '-' + this.normalizeMonthOrDayString(day);
    }

    public static getDateIntervalInTimestamp(dayStart: number, dayEnd: number) { // today is dayStart = 1, tomorrow is dayStart = 2 ...
        const date = new Date();
        const timestamp = date.getTime();
        const ms = date.getMilliseconds();
        const s = date.getSeconds();
        const m = date.getMinutes();
        const h = date.getHours();

        const dayStartInTimestamp = dayStart * (timestamp
            - this.hourToMilliSeconds(h)
            - this.minToMilliSeconds(m)
            - this.secondToMilliSeconds(s)
            - ms);
        const dayEndInTimestamp = dayStartInTimestamp + (dayEnd * this.hourToMilliSeconds(24));

        return [dayStartInTimestamp, dayEndInTimestamp];
    }

    public static getTimestamp() {
        return new Date().getTime() / 1000;
    }

    public static getDate() {
        return new Date().getDate();
    }

    public static dateToTimestamp(date: string) {
        return new Date(date).getTime() / 1000;
    }

    public static normalizeMonthOrDayString(monthOrDayValue: number) {
        const monthOrDayString = '' + monthOrDayValue;
        if (monthOrDayString.split('').length < 2) {
            return '0' + monthOrDayString;
        }

        return monthOrDayString;
    }

    public static setFavicon(img: HTMLImageElement) {
        const link = document.querySelector('link[rel*=\'icon\']') || document.createElement('link');
        link.type = 'image/x-icon';
        link.rel = 'shortcut icon';
        link.href = img;
        document.getElementsByTagName('head')[0].appendChild(link);
    }

    public static addClassToElement(elmt: string, className: string): void {
        $(elmt).addClass(className);
    }

    public static removeClassToElement(elmt: string, className: string): void {
        $(elmt).removeClass(className);
    }

    public static showCollapse(collapseId: string): void {
        $(collapseId).collapse('show');
    }

    public static isStringEmpty(string: string): boolean {
        return string === '';
    }

    public static checkEmail(email: string): boolean {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return re.test(email.toLowerCase());
    }

    public static isNull(elmt: any): boolean {
        return elmt === null;
    }

    public static isUndefined(elmt: any): boolean {
        return elmt === undefined;
    }

    public static isSet(elmt: any): boolean {
        return !this.isNull(elmt) && !this.isUndefined(elmt);
    }

    public static isInArray(arr: any[], elmt: any): boolean {
        return arr.indexOf(elmt) != -1;
    }

    public static hashStringMd5(string: string): string {
        const h = Crypto.createHash('md5');
        h.update(string);

        return h.digest('hex');
    }

    public static objectToString(object: Object): string {
        return JSON.stringify(object);
    }

    public static stringToObject(string: string): Object {
        return JSON.parse(string);
    }

    public static getSortedNumberArray(numberArray: number[]): number[] {
        return numberArray.sort((a: number, b: number): number => { return a - b });
    }
}
