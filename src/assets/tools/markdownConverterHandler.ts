import * as Showdown from 'showdown';

export default class MarkdownConverterHandler {

    protected showdown: Showdown.Converter;

    constructor() {
        this.showdown = new Showdown.Converter();
    }

    public toHtml = (markdownText: string): string => {
        return this.showdown.makeHtml(markdownText);
    }
}