// IMPORT APP SETTINGS ZONE
import AppSettings from '../../appSettings';
// END IMPORT APP SETTINGS ZONE


export default class LinkOpeningHandler {

    public static openLink = (link: string, target = '_blank', cordovaPluginOptions = ''): void => {
        if (AppSettings.settings.use_cordova) {
            window.open(link, target, cordovaPluginOptions);
        } else {
            window.open(link, target);
        }
    }
}