export default class CookieHandler {

    protected static COOKIE_PREFIX = "IA_";

    public static getCookie(cookieName: string): string {
        let name = this.COOKIE_PREFIX + cookieName + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');

        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];

            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }

            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }

        return "";
    }

    public static setCookie(cookieName: string, cookieValue: string, expirationDays: number): void {
        const d = new Date();
        d.setTime(d.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
        const expires = "expires=" + d.toUTCString();
        document.cookie = this.COOKIE_PREFIX + cookieName + "=" + cookieValue + ";" + expires + ";path=/";
    }

    public static deleteCookie(cookieName: string): void {
        document.cookie = this.COOKIE_PREFIX + cookieName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

}