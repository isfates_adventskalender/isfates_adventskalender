export default class SessionStorageHandler {

    protected static SESSIONSTORAGE_PREFIX = "IA_";

    public static getItem(itemName: string): string {
        return sessionStorage.getItem(this.SESSIONSTORAGE_PREFIX + itemName);
    }

    public static setItem(itemName: string, itemValue: string): void {
        sessionStorage.setItem(this.SESSIONSTORAGE_PREFIX + itemName, itemValue);
    }

    public static deleteItem(itemName: string): void {
        sessionStorage.removeItem(this.SESSIONSTORAGE_PREFIX + itemName);
    }

}