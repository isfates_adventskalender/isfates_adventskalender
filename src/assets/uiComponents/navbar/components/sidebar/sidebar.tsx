import * as React from 'react';

// IMPORT HELPER ZONE
// END IMPORT HELPER ZONE

// IMPORT COMPONENTS ZONE
import AppSettings from '../../../../../appSettings';
import ComponentBase from '../../../base/componentBase';
import SimpleNavItem from './components/simpleNavItem/simpleNavItem';
// END IMPORT COMPONENTS ZONE

// IMPORT STYLES ZONE
import './sidebar.scss';
// END IMPORT STYLES ZONE

// IMPORT APICMDS ZONE
import ApiCmds from '../../../../../api/apiCmds';
// END IMPORT APICMDS ZONE

// IMPORT LOCALIZE ZONE
import COMMON_LOCALIZE from '../../../../../commonLocalize';
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
// END IMPORT IMAGE ZONE

// IMPORT INTERFACE ZONE
import {
    TLanguages,
    ISetLangResponse,
    TPagesInMain,
    TFontawesomeIconClass,
    IUserData
} from '../../../../../commonInterface';
import IconHandler from '~assets/uiComponents/iconHandler/iconHandler';
// END IMPORT INTERFACE ZONE

// INTERFACE ZONE
interface IFlagsClass {
    en: string,
    de: string,
    fr: string,
}
// END INTERFACE ZONE


interface IProps {
    goToPage: (pageName: TPagesInMain) => void,
    setLanguage: (language: TLanguages) => void,
    language: TLanguages,
    userData: IUserData
    userName: string,
    userFirstName: string,
    userAvatarLink: string,
    logoutUser: () => void,
    showOrHideSidebar: () => void,
}

interface IState {
    showSidebar: boolean,
}

export default class Sidebar extends ComponentBase<IProps, IState> {

    protected componentId = 'sidebar';
    protected sidebar: JQuery;
    protected sidebarNav: JQuery;

    constructor(props: IProps) {
        super(props);

        this.state = {
            showSidebar: false,
        };
    }

    protected init = (): void => {
        this.initComponents();
    }

    protected initComponents = (): void => {
        this.sidebar = $(this.getId(this.getComponentId()));
        this.sidebarNav = $(this.getId('sidebarNav'));
    }

    protected setLanguage = (language: TLanguages): void => {
        this.props.setLanguage(language);

        const normalizeLangRequest = {
            lang: language,
        }

        ApiCmds.setLang(normalizeLangRequest,
            (result: ISetLangResponse) => {
                this.onSetLangCallback(result);
            });

        this.props.showOrHideSidebar();
    }

    protected onSetLangCallback(result: ISetLangResponse): void { }

    protected goToPage = (pageName: TPagesInMain): void => {
        this.props.goToPage(pageName);
        this.props.showOrHideSidebar();
    }

    protected pseudoHeaderRender() {
        return (
            <div
                id={this.setIdTo("pseudoHeader")}
                onClick={() => { this.goToPage('userProfilPage') }}
                className={"clickable"}
            >
                <div id={this.setIdTo("avatar")}>
                    <img src={this.props.userData.avatarLink} />
                </div>
                <h2 className="pseudo">
                    {this.props.userFirstName}
                </h2>
            </div>
        )
    }

    protected simpleNavItemRender(navItemText: string, navItemIcon: TFontawesomeIconClass, onClick?: () => void) {
        return (
            <SimpleNavItem
                text={navItemText}
                icon={navItemIcon}
                onClick={onClick}
            />
        )
    }


    render() {
        const commonLocalize = COMMON_LOCALIZE[this.props.language];
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setComponentId()} >
                <nav id={this.setIdTo("sidebarNav")} className="with-scrollbar">

                    {this.pseudoHeaderRender()}

                    <ul className="navbar-nav">
                        {this.simpleNavItemRender(localize['calendar'], 'gift', () => { this.goToPage('calendarPage') })}
                        {this.simpleNavItemRender(localize['myGifts'], 'list-ul', () => { this.goToPage('userGiftsPage') })}
                        {this.simpleNavItemRender(localize['sponsors'], 'handshake', () => { this.goToPage('sponsorsPage') })}
                        {this.simpleNavItemRender(localize.contact, 'phone', () => { this.goToPage('contactPage') })}
                        {this.simpleNavItemRender(commonLocalize.settings, 'cog', () => { this.goToPage('settingsPage') })}
                        {this.simpleNavItemRender(localize.logout, 'power-off', () => { this.props.logoutUser() })}
                    </ul>

                    <div id={this.setIdTo('appVersionCtn')}>
                        <p id={this.setIdTo('appVersion')}>&gt; Pre-Alpha</p>
                        <p id={this.setIdTo('appVersionTag')}>
                            <IconHandler icon="tag" className="with-pr reverse-h" />
                            v{AppSettings.settings.version}</p>
                    </div>
                </nav>
            </div>
        );
    }
}
