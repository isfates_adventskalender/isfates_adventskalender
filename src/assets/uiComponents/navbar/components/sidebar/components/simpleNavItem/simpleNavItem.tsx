import * as React from 'react';
import IconHandler from '../../../../../iconHandler/iconHandler';

// IMPORT STYLES ZONE
// END IMPORT STYLES ZONE

// IMPORT INTERFACE ZONE
import {
    TPages,
    TLanguages,
    ISetLangResponse,
    TPagesInMain,
    TFontawesomeIconClass
} from '../../../../../../../commonInterface';
// END IMPORT INTERFACE ZONE

// INTERFACE ZONE
// END INTERFACE ZONE


interface IProps {
    text: string,
    icon: TFontawesomeIconClass,
    onClick?: () => void,
}

export default class SimpleNavItem extends React.Component<IProps> {

    constructor(props: IProps) {
        super(props);
    }


    render() {
        return (
            <li className="nav-item">
                <a className="nav-link" onClick={this.props.onClick}>
                    <IconHandler icon={this.props.icon} />
                    {/*
                    <i className="fas fa-gift fa-fw">
                        {this.props.canGiftBeOpen
                            ? <span className="notification-badge"></span>
                            : ''
                        }

                    </i>
                    */}
                    {this.props.text}
                </a>
            </li>
        );
    }
}
