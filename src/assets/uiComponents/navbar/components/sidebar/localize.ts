const LOCALIZE = {
    "de": {
        logout: "Abmeldung",
        calendar: "Kalendar",
        contact: "Contact",
        myGifts: "Meine Geschenke",
        sponsors: "Sponsors",
    },
    "fr": {
        logout: "Déconnexion",
        calendar: "Calendrier",
        contact: "Contact",
        myGifts: "Mes cadeaux",
        sponsors: "Sponsors",
    },
    "en": {
        logout: "Logout",
        calendar: "Calendar",
        contact: "Contact",
        myGifts: "My gifts",
        sponsors: "Sponsors",
    },
};

export default LOCALIZE;