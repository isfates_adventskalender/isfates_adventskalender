import * as React from 'react';

// IMPORT HELPER ZONE
// END IMPORT HELPER ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../base/componentBase';
import Sidebar from './components/sidebar/sidebar';
// END IMPORT COMPONENTS ZONE

// IMPORT STYLES ZONE
import './navbar.scss';
// END IMPORT STYLES ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import AppIcon from '../../img/appIcon.svg';
// END IMPORT IMAGE ZONE

// IMPORT INTERFACE ZONE
import {
    TLanguages,
    TPagesInMain,
    IUserData,
} from '../../../commonInterface';
// END IMPORT INTERFACE ZONE

// INTERFACE ZONE
interface IFlagsClass {
    en: string,
    de: string,
    fr: string,
}
// END INTERFACE ZONE


interface IProps {
    goToPage: (pageName: TPagesInMain) => void,
    setLanguage: (language: TLanguages) => void,
    language: TLanguages,
    userData: IUserData,
    userName: string,
    userFirstName: string,
    userAvatarLink: string,
    logoutUser: () => void,
}

interface IState {
    showSidebar: boolean,
}

export default class Navbar extends ComponentBase<IProps, IState> {

    protected componentId = 'navbarCtn';
    protected sidebar: JQuery;
    protected sidebarNav: JQuery;
    protected toogleSidebarBtn: JQuery;
    protected toogleSidebarBurger: JQuery;

    constructor(props: IProps) {
        super(props);

        this.state = {
            showSidebar: false,
        };
    }

    protected init = (): void => {
        this.initPageComponents();
        this.initSidebarToogleHandling();
        this.initKeydownEvents();
    }

    protected initPageComponents = (): void => {
        this.sidebar = $("#sidebar"); // TODO: Don't call directly id like this
        this.sidebarNav = $('#sidebar_sidebarNav'); // TODO: Don't call directly id like this
        this.toogleSidebarBtn = $(this.getId("toogleSidebarBtn"));
        this.toogleSidebarBurger = $(this.getId("burger"));
    }

    protected initSidebarToogleHandling = (): void => {
        // Toogle sidebar when user click on navbar's burger
        this.toogleSidebarBtn.click((e: JQuery.ClickEvent) => {
            this.showOrHideSidebar();
        });

        // TODO: Don't use 'that' trick
        // Toogle sidebar when user click out of sidebar
        const that = this;
        this.sidebar.click(function (e) {
            if (e.target === this) {
                that.showOrHideSidebar();
            }
        });
    }

    protected initKeydownEvents = (): void => {
        window.onkeydown = (event: KeyboardEvent) => {
            if (event.keyCode === 27) {
                this.hideSidebar();
            }
        };
    }

    protected showOrHideSidebar = (): void => {
        // TODO: Fix bug -> When sidebar is open
        // and doubleclick on screen
        // sidebar is closed but burger still on "open" mode
        this.setState({
            showSidebar: !this.state.showSidebar,
        }, () => {
            if (this.state.showSidebar) {
                this.showSidebar();
            } else {
                this.hideSidebar();
            }
        });
    }

    protected showSidebar(): void {
        this.setState({
            showSidebar: true,
        });

        this.sidebarNav.css({
            animation: 'apparitionSidebarAnimation .5s forwards',
        })

        this.sidebar.show();
    }

    protected hideSidebar(): void {
        this.sidebarNav.css({
            animation: 'disparitionSidebarAnimation .5s forwards',
        })

        this.setState({
            showSidebar: false,
        });

        setTimeout(() => {
            this.sidebar.hide();

        }, 500);
    }


    protected goToPage = (pageName: TPagesInMain): void => {
        this.props.goToPage(pageName);
        this.showOrHideSidebar();
    }

    protected sidebarRender() {
        return (
            <Sidebar
                goToPage={this.props.goToPage}
                setLanguage={this.props.setLanguage}
                language={this.props.language}
                userData={this.props.userData}
                userName={this.props.userName}
                userFirstName={this.props.userFirstName}
                userAvatarLink={this.props.userAvatarLink}
                logoutUser={this.props.logoutUser}
                showOrHideSidebar={this.showOrHideSidebar}
            />
        );
    }


    render() {
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setComponentId()}>
                <nav id={this.setIdTo('navbar')} className="navbar navbar-dark bg-dark">
                    <button id={this.setIdTo('toogleSidebarBtn')} className="navbar-toggler" type="button" data-toggle="collapse">
                        <div className="box">
                            <div id={this.setIdTo("burger")} className={"hamburger " + (this.state.showSidebar ? "open" : "")}>
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </button>
                    <a className="navbar-brand">
                        <AppIcon />
                        <p>Adventskalender</p>
                    </a>
                </nav>

                {this.sidebarRender()}
            </div >
        );
    }
}
