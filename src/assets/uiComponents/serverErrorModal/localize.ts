const LOCALIZE = {
    de: {
        error_message: "Die Verbindung mit dem Server ist nicht möglich",
    },
    fr: {
        error_message: "Impossible de communiquer avec le serveur",
    },
    en: {
        error_message: "Impossible to communicate with the server",
    },
};

export default LOCALIZE;