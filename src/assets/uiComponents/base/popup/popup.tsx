import * as React from 'react';

import ComponentBase from '../componentBase';
import IconHandler from '../../iconHandler/iconHandler';

// IMPORT STYLES ZONE
import './popup.scss';
// END IMPORT STYLES ZONE


interface IProps {
    id: string,
    title: string,
    isOpen: boolean,
    onPopupClose: () => void,
}
interface IState { }

export default class Popup extends ComponentBase<IProps, IState> {

    protected componentId = "";

    constructor(props: IProps) {
        super(props);

        this.state = {};

        // TODO: This eventListener is global and will overwrite the default behavior (quit the app)
        // Maybe create a queue to handle all the goBackAction
        // document.addEventListener("backbutton", () => this.closePopup(), false);

        this._init();
    }

    protected ANIMATION_DURATION = 500;
    protected _init(): void {
        this.initKeydownEvents();
    }

    protected initKeydownEvents = (): void => {
        window.onkeydown = (event: KeyboardEvent) => {
            if (event.keyCode === 27) {
                this.props.onPopupClose();
            }
        };
    }

    protected getPopupClassName(): string {
        return 'popup ' + (this.props.isOpen ? 'show' : 'hidde');
    }

    protected popupHeaderRender = (): React.ReactNode => {
        return (
            <div className="popup-header">
                <IconHandler
                    icon="arrow-left"
                    className="go-back-icon clickable"
                    onClick={() => this.props.onPopupClose()}
                />

                <h1>{this.props.title}</h1>
            </div>
        )
    }


    render(): React.ReactNode {
        return (
            <div id={this.props.id} className={this.getPopupClassName()}>
                {this.popupHeaderRender()}

                <div className="popup-body with-scrollbar">
                    {this.props.children}
                </div>
            </div>
        )
    }
}