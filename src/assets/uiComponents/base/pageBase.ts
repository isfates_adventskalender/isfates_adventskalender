import * as React from 'react';
import {
    TPopups,
    TBootstrapPlugin
} from '~commonInterface';


export default abstract class PageBase<IProps, IState> extends React.Component<IProps, IState>  {

    constructor(props: IProps) {
        super(props);
    }

    componentDidMount(): void {
        this.init();
    }

    componentWillUnmount(): void {
        this.clear();
    }

    protected initBootstrapPlugins = (pluginsToInit: TBootstrapPlugin[]): void => {
        pluginsToInit.forEach((plugin: TBootstrapPlugin) => {
            switch (plugin) {
                case 'tooltips':
                    this.initBootstrapTooltipsPlugins();
            }
        })
    }

    protected initBootstrapTooltipsPlugins = (): void => {
        $((): void => {
            $('[data-toggle="tooltip"]').tooltip()
        });
    }

    protected abstract pageId: string;

    protected abstract init(): void;
    protected clear(): void { };

    protected getPageId(): string {
        return this.pageId;
    }

    protected setPageId() {
        return this.pageId;
    }

    protected setIdTo(id: string): string {
        return this.pageId + '_' + id;
    }

    protected getId(elementId: string, withSelector = true): string {
        const elmtId = this.pageId + '_' + elementId;

        if (withSelector) {
            return '#' + elmtId;
        }

        return elmtId;
    }

    protected openPopup(popupId: TPopups): void {
        const popupComponent = $('#' + popupId + '.popup');

        popupComponent.css({
            display: 'inline',
            animation: 'popupApparition 500ms',
        });
    }

    protected closePopup(popupId: TPopups): void {
        const popupComponent = $('#' + popupId + '.popup');

        popupComponent.css({
            animation: 'popupDisparition 500ms forwards',
        });
    }
}