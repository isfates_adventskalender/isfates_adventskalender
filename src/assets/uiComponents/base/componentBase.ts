import * as React from 'react';


export default abstract class ComponentBase<IProps, IState = {}> extends React.Component<IProps, IState>  {
    constructor(props: IProps) {
        super(props);
    }

    protected abstract componentId: string;

    protected init(): void { };
    protected clear(): void { };

    protected getComponentId(): string {
        return this.componentId;
    }

    protected setComponentId() {
        return this.componentId;
    }

    protected setIdTo(id: string): string {
        return this.componentId + "_" + id;
    }

    protected getId(elementId: string, withSelector = true): string {
        const elmtId = this.componentId + '_' + elementId;

        if (withSelector) {
            return '#' + elmtId;
        }

        return elmtId;
    }

    componentDidMount(): void {
        this.init();
    }

    componentWillUnmount(): void {
        this.clear();
    }
}