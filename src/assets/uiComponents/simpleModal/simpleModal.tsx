import * as React from 'react';
import ComponentBase from '../base/componentBase';

// IMPORT STYLES ZONE
import './simpleModal.scss';
// END IMPORT STYLES ZONE

// IMPORT INTERFACE ZONE
import { ISimpleModalParams } from '../../../commonInterface';
// END IMPORT INTERFACE ZONE

// INTERFACE ZONE
interface IProps {
    params: ISimpleModalParams,
}
// END INTERFACE ZONE

export default class SimpleModal extends ComponentBase<IProps> {

    protected componentId = 'simpleModal';

    constructor(props: IProps) {
        super(props);
    }

    protected simpleModalMessageRender = () => {
        if (this.props.params.isHtml) {
            return (
                <div dangerouslySetInnerHTML={{ __html: this.props.params.message }} />
            )
        }

        return (
            <div>{this.props.params.message}</div>
        )
    }


    render() {
        return (
            <div
                id={this.setComponentId()}
                className="modal fade"
                role="dialog"
                aria-labelledby="alertModalLabel"
                aria-hidden="true"
            >
                <div className="modal-dialog" role="document">
                    <div className={'alert alert-' + this.props.params.type} role="alert">
                        {this.simpleModalMessageRender()}
                    </div>
                </div>
            </div>
        );
    }
}
