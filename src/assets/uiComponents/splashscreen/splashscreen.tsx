import * as React from 'react';

// IMPORT HELPER ZONE
import Helper from '../../../helper';
// END IMPORT HELPER ZONE

// IMPORT STYLES ZONE
import './splashscreen.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import Icon from '../../../assets/img/icon.svg';
// END IMPORT IMAGE ZONE

// IMPORT COMPONENTS ZONE
// END IMPORT COMPONENTS ZONE

// INTERFACES ZONE
interface IProps {
    show: boolean,
}
// END INTERFACES ZONE


export default class Splashscreen extends ComponentBase<IProps> {

    protected componentId = 'splashscreen';
    protected splashscreen: JQuery;
    protected SPLASHSCREEN_FADEOUT_DURATION: number = 500;


    constructor(props: IProps) {
        super(props);
    }

    protected init = (): void => {
        this.splashscreen = $('#splashscreen');
    }

    protected showSplashscreen(): void {
        this.splashscreen.css({ display: "flex", animation: "none" });
    }

    protected hideSplashscreen(): void {
        this.splashscreen.css({ animation: `fadeOutAnimation ${Helper.milliSecondsToSeconds(this.SPLASHSCREEN_FADEOUT_DURATION)}s forwards` });

        setTimeout(() => {
            this.splashscreen.css({ display: "none" });
        }, this.SPLASHSCREEN_FADEOUT_DURATION);
    }


    componentDidUpdate(prevProps: IProps) {
        if (prevProps.show !== this.props.show) {
            if (this.props.show) {
                this.showSplashscreen();
            } else {
                this.hideSplashscreen();
            }
        }
    }


    render() {
        // TODO: Animate icon
        return (
            <div id={this.setComponentId()}>
                <Icon />
            </div>
        );
    }
}
