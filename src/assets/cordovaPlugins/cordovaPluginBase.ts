import AppSettings from '../../appSettings';

export default abstract class CordovaPluginBase {

    protected useCordova: boolean;

    constructor() {
        this.useCordova = AppSettings.settings.use_cordova;
        this.preInit();
    }

    protected preInit(): void {
        if (this.useCordova) {
            document.addEventListener("deviceready", () => this.init(), false);
        }
    }

    protected abstract init(): void;
}