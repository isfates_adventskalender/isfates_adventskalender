import CordovaPluginBase from './cordovaPluginBase';

// IMPORT INTERFACE ZONE
// END IMPORT INTERFACE ZONE

export default class InAppBrowserPlugin extends CordovaPluginBase {

    constructor() {
        super();
    }

    protected init = (): void => {
        //@ts-ignore
        window.open = cordova.InAppBrowser.open;
    }
}